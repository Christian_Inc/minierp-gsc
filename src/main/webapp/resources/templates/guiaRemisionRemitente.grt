// Factura de Venta
//      P: Configuracion de Pagina
//	T: Tipo de Documento
// 	W: Ancho del Reporte
// 	H: Alto del Reporte
// 	LM: Margen Izquierdo
// 	RM: Margen Derecho
// 	TM: Margen Superior
// 	BM: Margen Inferior
// 	F: Formato de pagina 

// Tipos de Documentos Disponibles {F: factura, B: boleta, GT: guia R. Transportista, R: reporte}
// Formatos disponibles {F: A4, F: A5}
// Si se Agrega formato el ancho y alto se ignoran

P: | T: F | W: 20.0 | H: 20.0 | LM: 2.0 | RM: 2.0 | TM: 2.0 | BM: 2.0

L: | $numfac:20 | $clicod:20 | $clinom:20 | $cliapepat:20 | $cliapemat:20 | $clinomcom:60
L: | #(DNI: ):20 | $vendni:20 | #{vendedor especial}:20 | $space:20 | #{Deje 20 espacios!}:20 
D: | 