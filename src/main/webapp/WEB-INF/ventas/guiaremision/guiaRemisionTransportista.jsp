<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir='/WEB-INF/tags'%>
<%@taglib prefix="cc" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<t:template-page-nav>
    <jsp:attribute name="mybody">
        <form id="formLote" role="form" action="" method="post">
            <div class="panel panel-default class">
                <div class="panel-heading">
                    <h1 class="text-left">Guias R. Transportista</h1>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <cc:if test = "${sessionScope.usuario.getTaGzzTipoUsuario().getTipUsuCod() == 1}">
                            <div class="col-md-12">
                                <div class="btn-group btn-group-justified" role="group" aria-label="First group">
                                    <div class="col-md-3 col-md-offset-6">
                                        <div class="form-group input-group" >
                                            <span class="input-group-addon">Tipo:</span>
                                            <select class="form-control" name="report" id="report">
                                                <option value="guiaRemision">Guía de Remisión</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 text-center">
                                        <button type="button" id="imprimir" 
                                                class="btn btn-primary btn-warning btn-block">Imprimir 
                                            <i class="fa fa-print"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <br />
                            <br />
                        </cc:if>

                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="tableGuiaTransportista">
                                    <thead>
                                        <tr>
                                            <cc:if test = "${sessionScope.usuario.getTaGzzTipoUsuario().getTipUsuCod()==1}">
                                                <th></th>
                                                </cc:if>
                                            <th class="text-center">Código</th>
                                            <th class="text-center">Fecha</th>
                                            <th class="text-center">Usuario</th>
                                            <th class="text-center">Transportista</th>
                                            <th class="text-center">Vehiculo</th>
                                            <th class="text-center">Ruta</th>
                                            <th class="text-center">Acción</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <cc:forEach items="${guias}" var="c">
                                            <tr>
                                                <cc:if test = "${sessionScope.usuario.getTaGzzTipoUsuario().getTipUsuCod()==1}">
                                                    <td width="3%" class="text-center">
                                                        <input type="checkbox" name="codigos" value="${c.guiTraCabId}">
                                                    </td>
                                                </cc:if>
                                                <td  nowrap="nowrap">${c.guiTraCabNum}</td>
                                                <td  nowrap="nowrap">
                                                    <fmt:formatDate value="${c.guiTraCabFecEmi}" pattern="dd/MM/yyyy" timeZone="UTC"/>
                                                </td>                                         
                                                <td  nowrap="nowrap">${c.enP1mUsuario.usuNom} ${c.enP1mUsuario.usuApePat} ${c.enP1mUsuario.usuApeMat}</td>                                         
                                                <td  nowrap="nowrap">${c.enP2mTransportista.traNom} ${c.enP2mTransportista.traApePat} ${c.enP2mTransportista.traApeMat}</td>                                         
                                                <td  nowrap="nowrap">${c.enP2mUnidadTransporte.uniTraNumPla}</td>                                         
                                                <td  nowrap="nowrap">${c.enP1mCatalogoRuta.catRutDet}</td>                                         
                                                <td>
                                                    <a onclick='viewCarrierGuide("${c.guiTraCabId}")' title="Vista Detallada de Guía R. Transportista">
                                                        <i class="fa fa-truck fa-lg" style="color: black;"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        </cc:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="impresionLotesModal" class="modal fade">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content" style="overflow-y: auto">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Impresión</h4>
                        </div>
                        <div class="modal-body">
                            <p align="center">Desea confirmar la impresion de las Guias R. Transportista?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-outline btn-success"> Imprimir </button>
                            <button type="button" class="btn btn-outline btn-danger" data-dismiss="modal"> Cancelar </button>
                        </div>
                    </div>              
                </div>
            </div>
        </form>


        <div class="modal fade" id="viewCarrierGuide">
            <div class="modal-dialog modal-md">
                <div class="modal-content" style="overflow-y: auto">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title">Guía de Remisión Transportista</h3>
                    </div>
                    <div class="modal-body">   
                        <div class="panel-body">
                            <ul class="nav nav-pills">
                                <li class="active"><a href="#generalguiTra" data-toggle="tab">Información General</a></li>
                                <li><a href="#detailguiTra" data-toggle="tab">Detalles</a></li>
                                <li><a href="#detailFacVen" data-toggle="tab">Facturas/Boletas</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="generalguiTra"><br>
                                    <div class="col-xs-12 col-md-12">
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group input-group">
                                                <span class="input-group-addon">Número de Guía de Transportista</span>
                                                <input type="text" class="form-control" id="guiTraCabNum" readOnly>
                                                <span class="input-group-addon"><i class="fa fa-clipboard"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group input-group">
                                                <span class="input-group-addon">Fecha Emision</span>
                                                <input type="text" class="form-control" id="fechaEmision" readOnly>
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group input-group">
                                                <span class="input-group-addon">Usuario Responsable</span>
                                                <input type="text" class="form-control" id="usuario" readOnly>
                                                <span class="input-group-addon"><i class="fa fa-child"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group input-group" >
                                                <span class="input-group-addon">Ruta</span>
                                                <input type="text" class="form-control" id="ruta" readOnly>
                                                <span class="input-group-addon"><i class="fa fa-road"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group input-group">
                                                <span class="input-group-addon">Transportista</span>
                                                <input type="text" class="form-control" id="transportista" readOnly>
                                                <span class="input-group-addon"><i class="fa fa-child"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group input-group">
                                                <span class="input-group-addon">Vehiculo</span>
                                                <input type="text" class="form-control" id="vehiculo" readOnly>
                                                <span class="input-group-addon"><i class="fa fa-car"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group input-group" >
                                                <span class="input-group-addon">Remitente(Empresa)</span>
                                                <input type="text" class="form-control" id="remitente" readOnly>
                                                <span class="input-group-addon"><i class="fa fa-building"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="detailguiTra"><br>
                                    <div class="col-xs-12 col-md-12">
                                        <div class="table-responsive">
                                            <table width="100%" class="table table-striped table-bordered table-hover" id="guiTransportistaTableDetails">
                                                <thead align="center">
                                                    <tr >
                                                        <th>Codigo</th>
                                                        <th>Detalle</th>
                                                        <th>Unidad</th>
                                                        <th>Cantidad</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="detailFacVen"><br>
                                    <div class="col-xs-12 col-md-12">
                                        <div class="form-group input-group" >
                                            <span class="input-group-addon">Total Guia R. Transportista</span>
                                            <input type="text" class="form-control" id="total" readOnly>
                                            <span class="input-group-addon"><i class="fa fa-bank"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-12">
                                        <div class="table-responsive">
                                            <table width="100%" class="table table-striped table-bordered table-hover" id="guiTransportistaTableFacts">
                                                <thead align="center">
                                                    <tr >
                                                        <th>Código</th>
                                                        <th>Modalidad</th>
                                                        <th>Total Facturado</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">Aceptar</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="errorMessageModal" class="modal fade">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" style="overflow-y: auto">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Error</h4>
                    </div>
                    <div class="modal-body">
                        <p align="center"><span id="errorMessage"></span></p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" data-dismiss="modal">Aceptar</button>                                            
                    </div>
                </div>         
            </div>
        </div>
    </jsp:attribute>

    <jsp:attribute name="myscripts">
        <script language="javascript">

            $(document).ready(function () {
                $('#tableGuiaTransportista').DataTable({
                    responsive: true
                });
            });

            $(document).ready(function () {
                $('#imprimir').on('click', function () {
                    $('#formLote').attr('action', '${pageContext.request.contextPath}/secured/ventas/factura/facturaLotes');
                    if ($(':checkbox:checked').length > 0)
                    {
                        $('#impresionLotesModal').modal('show');
                    } else
                    {
                        $("#errorMessage").text("Debe seleccionar al menos una Guia R. Transportista!");
                        $('#errorMessageModal').modal('show');
                    }
                });
            });

            function viewCarrierGuide(guiTraCabId) {
                $.post(
                        "${pageContext.request.contextPath}/secured/ventas/SearchGuiaTransportista", {
                            action: "view",
                            guiTraCabId: guiTraCabId
                        }
                ).done(function (data) {
                    $("#guiTraCabId").val(data.guiTraCabId);
                    $("#guiTraCabNum").val(data.guiTraCabNum);
                    $("#guiTraCabNum").val(data.guiTraCabNum);
                    $("#usuario").val(data.usuario);
                    $("#ruta").val(data.ruta);
                    $("#transportista").val(data.transportista);
                    $("#vehiculo").val(data.vehiculo);
                    $("#remitente").val(data.remitente);
                    $("#fechaEmision").val(data.fechaEmision);
                    $("#total").val(data.total);

                    $('#guiTransportistaTableDetails').DataTable().clear().draw();
                    $('#guiTransportistaTableDetails').DataTable().destroy();
                    data.detallesList.forEach(function (detail) {
                        $('#guiTransportistaTableDetails tbody').append('<tr><td width="15%" align="center"></td><td width="50%" align="center"></td><td width="15%" align="center"></td><td width="20%"></td></tr>');
                        $('#guiTransportistaTableDetails tr:last td:eq(0)').html(detail.guiTraCod);
                        $('#guiTransportistaTableDetails tr:last td:eq(1)').html(detail.guiTraDet);
                        $('#guiTransportistaTableDetails tr:last td:eq(2)').html(detail.guiTraUniMed);
                        $('#guiTransportistaTableDetails tr:last td:eq(3)').html(Number(detail.guiTraCan).toFixed(2));
                    });
                    $('#guiTransportistaTableDetails').DataTable({
                        responsive: true
                    });

                    $('#guiTransportistaTableFacts').DataTable().clear().draw();
                    $('#guiTransportistaTableFacts').DataTable().destroy();
                    data.facturasList.forEach(function (detail) {
                        $('#guiTransportistaTableFacts tbody').append('<tr><td width="40%" align="center"></td><td width="30%"></td><td width="30%" align="center"></tr>');
                        $('#guiTransportistaTableFacts tr:last td:eq(0)').html(detail.facNum);
                        $('#guiTransportistaTableFacts tr:last td:eq(1)').html(detail.facMod);
                        $('#guiTransportistaTableFacts tr:last td:eq(2)').html(Number(detail.facSubTot).toFixed(2));
                    });
                    $('#guiTransportistaTableFacts').DataTable({
                        responsive: true
                    });

                    $("#viewCarrierGuide").modal('show');
                });
            }

        </script>
    </jsp:attribute>
</t:template-page-nav>