<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir='/WEB-INF/tags'%>
<%@taglib prefix="cc" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<t:template-page-nav>
    <jsp:attribute name="mybody">
        <div class="panel panel-default class">
            <div class="panel-heading">
                <h1 class="text-left">Listas de Precios</h1>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="formLoadListaPrecios" role="form" method="post" action="${pageContext.request.contextPath}/secured/ventas/PrecioUnitario">
                            <input type="hidden" name="accion" value="load">
                            <div class="btn-group btn-group-justified" role="group" aria-label="First group">
                                <div class="col-md-6 text-center">
                                    <div class="form-group input-group" >
                                        <span class="input-group-addon">Lista de Precios: </span>
                                        <select class="form-control" name="lisPreCod" id="lisPreCodSelect">
                                            <cc:forEach items="${listasPrecios}" var="lp">
                                                <option value="${lp.lisPreCod}">${lp.lisPreDet}</option>
                                            </cc:forEach>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3 col-md-offset-3 text-center">
                                    <button type="submit" class="btn btn-success btn-block">Cargar Lista de Precios 
                                        <i class="fa fa-book"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <br />
                    <br />
                    <br />
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table id="tableListaPrecios" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center">Clase de Producto</th>
                                        <th class="text-center">SubClase de Producto</th>
                                        <th class="text-center">Codigo de Producto</th>
                                        <th class="text-center">Detalle de Producto</th>
                                        <th class="text-center">Valor de Venta</th>
                                        <th class="text-center">Operaciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <cc:forEach items="${productos}" var="p">
                                        <tr>
                                            <td nowrap="nowrap" class="text-center">${p.id.claProCod}</td>
                                            <td nowrap="nowrap" class="text-center">${p.id.subClaProCod}</td>
                                            <td nowrap="nowrap" class="text-center">${p.id.proCod}</td>
                                            <td nowrap="nowrap">${p.enP2mProducto.proDet}</td>
                                            <td nowrap="nowrap">
                                                <fmt:formatNumber type="number" 
                                                                  minFractionDigits="2" 
                                                                  maxFractionDigits="2" 
                                                                  value="${p.preUniVen}" />
                                            </td>
                                            <td>
                                                <a href="#" data-toggle="modal" data-target="#updateModal" title="Modificar Precio Unitario"
                                                   data-lisprecod="${p.id.lisPreCod}" 
                                                   data-claprocod="${p.id.claProCod}" 
                                                   data-subclaprocod="${p.id.subClaProCod}" 
                                                   data-procod="${p.id.proCod}" 
                                                   data-prodet="${p.enP2mProducto.proDet}" 
                                                   data-preuniven="<fmt:formatNumber type="number" 
                                                                     minFractionDigits="2" 
                                                                     maxFractionDigits="2" 
                                                                     value="${p.preUniVen}" />" 
                                                   data-preunicom="${p.preUniCom}" 
                                                   data-preunimar="${p.preUniMar}"
                                                   data-preunifle="${p.preUniFle}"
                                                   data-lispredet="${p.taGzzListaPrecios.lisPreDet}">
                                                    <i class="fa fa-pencil fa-lg" style="color: black;"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    </cc:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> 
            </div>
        </div>

        <div id="updateModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content" style="overflow-y: auto">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Actualizar Precio</h4>
                    </div>
                    <form id="formUpdatePrecioVenta" method="post" action="${pageContext.request.contextPath}/secured/ventas/PrecioUnitario">
                        <div class="modal-body">
                            <input type="hidden" name="accion" value="update">
                            <input type="hidden" name="lisPreCod" id="updateLisPreCod">
                            <input type="hidden" name="claProCod" id="updateClaProCod">
                            <input type="hidden" name="subClaProCod" id="updateSubClaProCod">
                            <input type="hidden" name="proCod" id="updateProCod">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label>Lista de Precios:</label>
                                        <input type="text" class="form-control" name="lisPreDet" id="updateLisPreDet" readonly>
                                    </div>
                                    <div class="col-sm-9">
                                        <label>Producto:</label>
                                        <input type="text" class="form-control" name="proDet" id="updateProDet" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <label>Precio Venta:</label>
                                        <input type="text" class="form-control" name="preUniVen" id="updatePreUniVen" readonly>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Precio Compra:</label>
                                        <input type="number" class="form-control" name="preUniCom" id="updatePreUniCom"
                                               min="0.0" step="any">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <label>Precio Margen:</label>
                                        <input type="number" class="form-control" name="preUniMar" id="updatePreUniMar"
                                               min="0.0" step="any">
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Precio Flete:</label>
                                        <input type="number" class="form-control" name="preUniFle" id="updatePreUniFle"
                                               min="0.0" step="any">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-outline btn-success">Actualizar</button>
                            <button type="button" class="btn btn-outline btn-danger" data-dismiss="modal">Cancelar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </jsp:attribute>

    <jsp:attribute name="myscripts">
        <script>
            $(document).ready(function () {
                $('#tableListaPrecios').DataTable({
                    responsive: true
                });
            });

            var updateLisPreCod = $("#updateLisPreCod");
            var updateClaProCod = $("#updateClaProCod");
            var updateSubClaProCod = $("#updateSubClaProCod");
            var updateProCod = $("#updateProCod");
            var updateProDet = $("#updateProDet");
            var updatePreUniVen = $("#updatePreUniVen");
            var updatePreUniCom = $("#updatePreUniCom");
            var updatePreUniMar = $("#updatePreUniMar");
            var updatePreUniFle = $("#updatePreUniFle");
            var updateLisPreDet = $("#updateLisPreDet");
            var updateModal = $("#updateModal");

            updateModal.on('show.bs.modal', function (e) {
                updateLisPreCod.val($(e.relatedTarget).data('lisprecod'));
                updateClaProCod.val($(e.relatedTarget).data('claprocod'));
                updateSubClaProCod.val($(e.relatedTarget).data('subclaprocod'));
                updateProCod.val($(e.relatedTarget).data('procod'));
                updateProDet.val($(e.relatedTarget).data('prodet'));
                updatePreUniVen.val($(e.relatedTarget).data('preuniven'));
                updatePreUniCom.val($(e.relatedTarget).data('preunicom'));
                updatePreUniMar.val($(e.relatedTarget).data('preunimar'));
                updatePreUniFle.val($(e.relatedTarget).data('preunifle'));
                updateLisPreDet.val($(e.relatedTarget).data('lispredet'));
            });
        </script>
    </jsp:attribute>
</t:template-page-nav>