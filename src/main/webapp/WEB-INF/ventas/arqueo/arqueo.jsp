<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir='/WEB-INF/tags'%>
<%@taglib prefix="cc" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<t:template-page-nav>
    <jsp:attribute name="mybody">
        <div class="panel panel-default class">
            <div class="panel-heading">
                <h1 class="text-left">Arqueo de Caja</h1>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">    
                        <form id="arqueoModal" method="post" action="${pageContext.request.contextPath}/secured/ventas/arqueoCaja">
                            <div class="col-md-6">
                                <div class="form-group input-group" >
                                    <span class="input-group-addon">Usuario</span>
                                    <select class="form-control" name="usuCod">
                                        <cc:if test = "${sessionScope.usuario.getTaGzzTipoUsuario().getTipUsuCod()!=2}">    
                                            <option value="-1">Todos</option>
                                        </cc:if>
                                        <cc:forEach var="u" items="${usuarios}">
                                            <option value="${u.usuCod}">${u.usuApePat} ${u.usuApeMat}, ${u.usuNom}</option>
                                        </cc:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group input-group" >
                                    <span class="input-group-addon">Inicio:</span>
                                    <input type="date" class="form-control" name="fecIni" value="${fecIni}">
                                </div>
                            </div>
                            <div class="col-m-3">
                                <div class="form-group input-group" >
                                    <span class="input-group-addon">Fin:</span>
                                    <input type="date" class="form-control" name="fecFin" value="${fecFin}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-4 ">
                                        <button type="submit" class="btn btn-block btn-success">Buscar <i class="fa fa-eye"></i>  </button>
                                    </div>
                                </div>   
                            </div>
                        </form>
                    </div>       
                </div>

                <br />
                <br />
                <br />

                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover" id="tablaMovimientos">
                            <thead>
                                <tr>
                                    <th class="text-center">Código</th>
                                    <th class="text-center">Usuario</th>
                                    <th class="text-center">Valor Neto</th>
                                    <th class="text-center">Valor IGV</th>
                                    <th class="text-center">Valor Total </th>
                                </tr>
                            </thead>
                            <tbody>
                                <cc:forEach var="c" items="${consulta}">
                                    <tr>
                                        <td nowrap="nowrap" class="text-center">${c[0]}</td>
                                        <td nowrap="nowrap">${c[3]} ${c[1]} ${c[2]}</td>
                                        <td class="text-center">
                                            <fmt:formatNumber type="number" 
                                                              minFractionDigits="2" 
                                                              maxFractionDigits="2" 
                                                              value="${c[5]}" />
                                        </td>
                                        <td class="text-center">
                                            <fmt:formatNumber type="number" 
                                                              minFractionDigits="2" 
                                                              maxFractionDigits="2" 
                                                              value="${c[4]}" />
                                        </td>
                                        <td class="text-center">
                                            <fmt:formatNumber type="number" 
                                                              minFractionDigits="2" 
                                                              maxFractionDigits="2" 
                                                              value="${c[5] + c[4]}" />
                                        </td>
                                    </tr>
                                </cc:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>                
            </div>
        </div>
    </jsp:attribute>

    <jsp:attribute name="myscripts">
        <script>
            $(document).ready(function () {
                $('#tablaMovimientos').DataTable({
                    responsive: true
                });
            });
        </script>                        
    </jsp:attribute>
</t:template-page-nav>