package org.epis.minierp.util.reporter;

import com.itextpdf.layout.Style;
import java.io.IOException;
import java.util.ArrayList;

public class ITextReport {



    private ITextUtil iText;

    public ITextReport(String path, String name) {
        this.iText = new ITextUtil(path, name );
    }

    public ITextReport(String path, String name, float width, float height, float leftMargin,
            float rightMargin, float topMargin, float bottonMargin) {

        this.iText = new ITextUtil(path, name , width, height, leftMargin, rightMargin, topMargin, bottonMargin);
    }

    public ITextReport(String path, String name, double width_cm, double height_cm, double leftMargin,
            double rightMargin, double topMargin, double bottonMargin) {

        this.iText = new ITextUtil(path, name ,width_cm, height_cm, leftMargin, rightMargin, topMargin, bottonMargin);
    }

    public void close() {
        iText.closeDocument();
    }

    public void addLine(ArrayList<ITextToken> tokens, Style myStyle) {
        String content = "";
        String tempContent;
        String add = "";
        int sizeContent;
        for (ITextToken token : tokens) {
            tempContent = token.getContent();
            sizeContent = token.getSize();
            if (tempContent.length() > sizeContent) {
                tempContent = tempContent.substring(0, sizeContent);
            } else {
                for (int i = 0; i < sizeContent - tempContent.length(); i++) {
                    add = add + " ";
                }
                tempContent = tempContent + add;
            }
            content = content + tempContent;
        }
        iText.addParagraph(content, myStyle, ITextUtil.ALIGN_LEFT);
    }

    public void addLine(ITextToken token, Style myStyle) {
        String content;
        String add = "";
        int size;
        content = token.getContent();
        size = token.getSize();
        if (content.length() > size) {
            content = content.substring(0, size);
        } else {
            for (int i = 0; i < size - content.length(); i++) {
                add = add + " ";
            }
            content = content + add;
        }
        iText.addParagraph(content, myStyle, ITextUtil.ALIGN_LEFT);
    }

    public void addLineJump() {
        iText.addLineJump();
    }

    /**
     * La Impresora debe ser un recurso compartido
     *
     * @param printerName Nombre de la Impresora como recurso compartido
     */
    public void sendToPrinter(String printerName) {
        try {
            String ipAddress = "localhost";
            String file = iText.getAbsolutePath();

            String printCmd = "print /d:\\\\" + ipAddress + "\\" + printerName + " \"" + file + "\"";
            Runtime.getRuntime().exec(printCmd);
        } catch (IOException ex) {
        }
    }

}
