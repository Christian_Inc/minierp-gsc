package org.epis.minierp.util.reporter;

import java.util.Objects;

public class ITextToken {

    private int size;
    private String content;

    public ITextToken() {
    }

    public ITextToken(int size, String content) {
        this.size = size;
        this.content = content;
    }

    public int getSize() {
        return size;
    }

    public String getContent() {
        return content;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "TokenPdf{" + "size=" + size + ", content=" + content + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + this.size;
        hash = 23 * hash + Objects.hashCode(this.content);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ITextToken other = (ITextToken) obj;
        if (this.size != other.size) {
            return false;
        }
        if (!Objects.equals(this.content, other.content)) {
            return false;
        }
        return true;
    }

    public static ITextToken tokenEmpty(int size) {
        String content = "";
        for (int i = 0; i < size; i++) {
            content = content + " ";
        }
        return new ITextToken(size, content);
    }
}
