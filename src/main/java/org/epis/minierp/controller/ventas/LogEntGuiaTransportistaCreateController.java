package org.epis.minierp.controller.ventas;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.epis.minierp.business.logistica.LogEntGuiaTransportistaBusiness;
import org.epis.minierp.dao.general.EnP1mEmpresaDao;
import org.epis.minierp.model.EnP1mEmpresa;
import org.epis.minierp.util.DateUtil;

public class LogEntGuiaTransportistaCreateController extends HttpServlet {

    EnP1mEmpresaDao empDao;
    LogEntGuiaTransportistaBusiness guiTraBusiness;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        empDao = new EnP1mEmpresaDao();
        guiTraBusiness = new LogEntGuiaTransportistaBusiness();
        EnP1mEmpresa myEmpresa = empDao.getById(01);
        int numMaxGuiTraDets = myEmpresa.getEmpNumDetGuiRemTra();

        String[] codigos = request.getParameterValues("codigos");
        int[] facVenCabCods = new int[codigos.length];
        for (int i = 0; i < codigos.length; i++) {
            facVenCabCods[i] = Integer.parseInt(codigos[i]);
        }

        String guiTraCabNumIni, usuCod, traCod, uniTraCod;
        Date guiTraCabFecEmi, guiTraCabFecVen;
        int motTraCod, catRutCod, numGuiTraGen;

        guiTraCabNumIni = request.getParameter("guiTraCabNumIni");
        usuCod = request.getParameter("guiTraUsuCod");
        traCod = request.getParameter("guiTraTraCod");
        uniTraCod = request.getParameter("guiTraUniTraCod");

        guiTraCabFecEmi = DateUtil.getDate2String(request.getParameter("guiTraCabFecEmi"));
        guiTraCabFecVen = DateUtil.getDate2String(request.getParameter("guiTraCabFecVen"));

        motTraCod = Integer.parseInt(request.getParameter("guiTraMotTraCod"));

        catRutCod = Integer.parseInt(request.getParameter("guiTraCatRutCod"));

        numGuiTraGen = guiTraBusiness.create4Ventas(facVenCabCods, guiTraCabNumIni, guiTraCabFecEmi,
                guiTraCabFecVen, 1, usuCod, traCod, uniTraCod, motTraCod, catRutCod, numMaxGuiTraDets);

        response.sendRedirect(request.getContextPath() + "/secured/ventas/factura");
    }
}
