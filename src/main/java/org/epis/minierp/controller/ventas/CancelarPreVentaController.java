package org.epis.minierp.controller.ventas;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.epis.minierp.business.ventas.EnP1mPreventaBusiness;
import org.epis.minierp.dao.logistica.EnP2mProductoDao;
import org.epis.minierp.dao.ventas.EnP1mPreventaCabDao;
import org.epis.minierp.model.EnP1mPreventaCab;
import org.epis.minierp.model.EnP1tPreventaDet;
import org.epis.minierp.model.EnP2mProducto;
import org.epis.minierp.util.BigDecimalUtil;

public class CancelarPreVentaController extends HttpServlet {

    private static final long serialVersionUID = 1L;
    EnP1mPreventaBusiness preventaBusiness;
    EnP1mPreventaCabDao preVenCabDao;
    EnP2mProductoDao proDao;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        preventaBusiness = new EnP1mPreventaBusiness();
        preVenCabDao = new EnP1mPreventaCabDao();
        proDao = new EnP2mProductoDao();

        int preVenCabCod = Integer.parseInt(request.getParameter("preVenCabCod"));

        EnP1mPreventaCab myPreVenCab = preVenCabDao.getById(preVenCabCod);
        myPreVenCab.setEstRegCod('I');
        preVenCabDao.update(myPreVenCab);

        List<EnP1tPreventaDet> detalles = new ArrayList<>(myPreVenCab.getEnP1tPreventaDets());

        EnP2mProducto temp;
        for (EnP1tPreventaDet det : detalles) {
            temp = det.getEnP2mProducto();
            temp.setProStkPreVen(temp.getProStkPreVen() - BigDecimalUtil.get(det.getPreVenDetCan()));
            proDao.update(temp);
        }

        response.sendRedirect(request.getContextPath() + "/secured/ventas/preventa");
    }
}
