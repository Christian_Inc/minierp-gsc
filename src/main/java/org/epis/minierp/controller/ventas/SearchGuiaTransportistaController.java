package org.epis.minierp.controller.ventas;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.epis.minierp.dao.logistica.LogEntGuiaTransportistaCabDao;
import org.epis.minierp.model.LogCtrGuiaTransportistaDet;
import org.epis.minierp.model.LogCtrGuiaTransportistaFacturas;
import org.epis.minierp.model.LogEntGuiaTransportistaCab;
import org.epis.minierp.util.BigDecimalUtil;
import org.epis.minierp.util.DateUtil;

public class SearchGuiaTransportistaController extends HttpServlet {

    LogEntGuiaTransportistaCabDao guiTraCabDao;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        guiTraCabDao = new LogEntGuiaTransportistaCabDao();
        int guiTraCabId = Integer.parseInt(request.getParameter("guiTraCabId"));
        String action = request.getParameter("action");

        LogEntGuiaTransportistaCab myGuiaTransportista;
        JsonObject data = new JsonObject();

        JsonArray detallesList = new JsonArray();
        JsonArray facturasList = new JsonArray();

        String usuario, ruta, transportista, vehiculo, remitente, fechaEmision;
        BigDecimal tempSubTot;
        BigDecimal total = BigDecimal.ZERO;

        switch (action) {
            case "view": {
                myGuiaTransportista = guiTraCabDao.getById(guiTraCabId);
                if (myGuiaTransportista != null) {
                    usuario = myGuiaTransportista.getEnP1mUsuario().getUsuNom() + " "
                            + myGuiaTransportista.getEnP1mUsuario().getUsuApePat() + " "
                            + myGuiaTransportista.getEnP1mUsuario().getUsuApeMat();

                    ruta = myGuiaTransportista.getEnP1mCatalogoRuta().getCatRutDet();

                    transportista = myGuiaTransportista.getEnP2mTransportista().getTraNom() + " "
                            + myGuiaTransportista.getEnP2mTransportista().getTraApePat() + " "
                            + myGuiaTransportista.getEnP2mTransportista().getTraApeMat();

                    vehiculo = myGuiaTransportista.getEnP2mUnidadTransporte().getUniTraMar() + " / "
                            + myGuiaTransportista.getEnP2mUnidadTransporte().getUniTraMod() + " / "
                            + myGuiaTransportista.getEnP2mUnidadTransporte().getUniTraNumPla();

                    remitente = myGuiaTransportista.getEnP1mEmpresa().getEmpRazSoc();

                    fechaEmision = DateUtil.convertDateToString(myGuiaTransportista.getGuiTraCabFecEmi());

                    data.addProperty("guiTraCabId", myGuiaTransportista.getGuiTraCabId());
                    data.addProperty("guiTraCabNum", myGuiaTransportista.getGuiTraCabNum());
                    data.addProperty("usuario", usuario);
                    data.addProperty("ruta", ruta);
                    data.addProperty("transportista", transportista);
                    data.addProperty("vehiculo", vehiculo);
                    data.addProperty("remitente", remitente);
                    data.addProperty("fechaEmision", fechaEmision);

                    List<LogCtrGuiaTransportistaDet> details = new ArrayList<>(myGuiaTransportista.getLogCtrGuiaTransportistaDets());

                    for (LogCtrGuiaTransportistaDet itm : details) {
                        JsonObject detailObject = new JsonObject();
                        detailObject.addProperty("guiTraCod", itm.getEnP2mProducto().getId().getProCod());
                        detailObject.addProperty("guiTraDet", itm.getEnP2mProducto().getProDet());
                        detailObject.addProperty("guiTraUniMed", itm.getEnP2mProducto().getTaGzzUnidadMed().getUniMedSim());
                        detailObject.addProperty("guiTraCan", itm.getGuiTraDetNumItm());
                        detallesList.add(detailObject);
                    }

                    List<LogCtrGuiaTransportistaFacturas> facts = new ArrayList<>(myGuiaTransportista.getLogCtrGuiaTransportistaFacturases());

                    for (LogCtrGuiaTransportistaFacturas itm : facts) {
                        JsonObject detailObject = new JsonObject();
                        detailObject.addProperty("facNum", itm.getEnP1mFacturaVentaCab().getFacVenCabNum());
                        detailObject.addProperty("facMod", itm.getEnP1mFacturaVentaCab().getFacVenCabModVen());
                        //IGV + NETO
                        tempSubTot = BigDecimalUtil.sumar(itm.getEnP1mFacturaVentaCab().getFacVenCabSubTot(), itm.getEnP1mFacturaVentaCab().getFacVenCabTot());
                        detailObject.addProperty("facSubTot", tempSubTot);
                        //Total Guia R. Transportista
                        total = BigDecimalUtil.sumar(total, tempSubTot);
                        facturasList.add(detailObject);
                    }

                    data.addProperty("total", total);

                } else {
                    data.addProperty("guiTraCabId", "Desconocido");
                    data.addProperty("guiTraCabNum", "Desconocido");
                    data.addProperty("usuario", "Desconocido");
                    data.addProperty("ruta", "Desconocido");
                    data.addProperty("transportista", "Desconocido");
                    data.addProperty("vehiculo", "Desconocido");
                    data.addProperty("remitente", "Desconocido");
                    data.addProperty("fechaEmision", "Desconocido");
                    data.addProperty("total", 0);

                }

                data.add("detallesList", detallesList);
                data.add("facturasList", facturasList);

                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(new Gson().toJson(data));
            }
        }
    }

}
