package org.epis.minierp.controller.ventas;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.epis.minierp.dao.general.EnP1mUsuarioDao;
import org.epis.minierp.dao.general.TaGzzListaPreciosDao;
import org.epis.minierp.model.EnP1mUsuario;
import org.epis.minierp.model.TaGzzListaPrecios;

public class PerfilListaPreciosController extends HttpServlet {

    private EnP1mUsuarioDao usuDao;
    private TaGzzListaPreciosDao lisPreDao;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        usuDao = new EnP1mUsuarioDao();
        lisPreDao = new TaGzzListaPreciosDao();
        String usuCod = request.getParameter("usuCod");
        try {
            int lisPreCod = Integer.parseInt(request.getParameter("lisPreCod"));
            EnP1mUsuario myUsuario = usuDao.getById(usuCod);
            TaGzzListaPrecios myListaPrecios = lisPreDao.getById(lisPreCod);
            myUsuario.setTaGzzListaPrecios(myListaPrecios);
            usuDao.update(myUsuario);

            //actualizando el token de usuario en linea
            HttpSession session = request.getSession(true);
            session.setAttribute("usuario", myUsuario);
        } catch (Exception e) {
        } finally {
            response.sendRedirect(request.getContextPath() + "/secured/general/perfil");
        }
    }
}
