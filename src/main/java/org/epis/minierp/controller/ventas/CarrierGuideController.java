package org.epis.minierp.controller.ventas;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.epis.minierp.business.ventas.EnP1mFacturaVentaBusiness;
import org.epis.minierp.dao.general.EnP1mEmpresaDao;
import org.epis.minierp.dao.general.EnP2mUnidadTransporteDao;
import org.epis.minierp.dao.general.TaGzzTipoDestinatarioDao;
import org.epis.minierp.dao.logistica.EnP2mTransportistaDao;
import org.epis.minierp.dao.ventas.EnP1mClientesRutasDao;
import org.epis.minierp.dao.ventas.EnP1mFacturaVentaCabDao;
import org.epis.minierp.model.EnP1mCliente;
import org.epis.minierp.model.EnP1mClientesRutas;
import org.epis.minierp.model.EnP1mEmpresa;
import org.epis.minierp.model.EnP1mFacturaVentaCab;
import org.epis.minierp.model.EnP2mTransportista;
import org.epis.minierp.model.EnP2mUnidadTransporte;
import org.epis.minierp.model.LogEntGuiaTransportistaCab;
import org.epis.minierp.model.TaGzzTipoDestinatario;

public class CarrierGuideController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        EnP1mEmpresa empresa = (new EnP1mEmpresaDao()).getById(01);
        EnP1mFacturaVentaBusiness facVenBuss = new EnP1mFacturaVentaBusiness();
        int empNumDec = empresa.getEmpNumDec();
        String action = request.getParameter("action");
        int facVenCabCod = Integer.parseInt(request.getParameter("facVenCabCod"));
        String guiRemTraNum = request.getParameter("guiRemTraNum");
        EnP1mFacturaVentaCab bill = null;
        LogEntGuiaTransportistaCab carrierGuide = null;
        JsonObject data = null;
        JsonArray traList = null;
        JsonArray traCliList = null;
        JsonArray facVenList = null;

        switch (action) {
            case "search":
                bill = (new EnP1mFacturaVentaCabDao()).getById(facVenCabCod);
                carrierGuide = null;
                data = new JsonObject();
                traList = new JsonArray();
                traCliList = new JsonArray();
                facVenList = new JsonArray();

                data.addProperty("guiRemTraNum", "No Generado");
                data.addProperty("traNomCom", "No Generado");
                data.addProperty("vehiculo", "No Generado");
                data.addProperty("remitente", "No Generado");
                data.addProperty("destinatario", "No Generado");
                data.addProperty("ruta", "No Generado");
                data.add("traList", traList);
                data.add("traCliList", traCliList);
                data.add("facVenList", facVenList);

                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(new Gson().toJson(data));
                break;

            case "view":

                data.addProperty("guiRemTraNum", "No Generado");
                data.addProperty("traNomCom", "No Generado");
                data.addProperty("vehiculo", "No Generado");
                data.addProperty("remitente", "No Generado");
                data.addProperty("destinatario", "No Generado");
                data.addProperty("ruta", "No Generado");
                data.add("traList", traList);
                data.add("traCliList", traCliList);
                data.add("facVenList", facVenList);

                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(new Gson().toJson(data));
                break;

            case "verify":
                boolean state = (new EnP1mFacturaVentaCabDao()).verifyCarrierGuide(facVenCabCod);
                EnP1mEmpresa company = (new EnP1mEmpresaDao()).getById(1);
                bill = (new EnP1mFacturaVentaCabDao()).getById(facVenCabCod);
                data = new JsonObject();

                data.addProperty("state", state);
                data.addProperty("empDes", company.getEmpDes());
                data.addProperty("facCod", facVenCabCod);

                JsonArray carriers = new JsonArray();
                List<EnP2mTransportista> transportistas = (new EnP2mTransportistaDao()).getAllActive();

                for (EnP2mTransportista transportista : transportistas) {
                    JsonObject carrier = new JsonObject();
                    carrier.addProperty("traCod", transportista.getTraCod());
                    carrier.addProperty("nomCom", transportista.getTraNomCom());
                    carriers.add(carrier);
                }

                data.add("traDat", carriers);

                JsonArray recipientTypes = new JsonArray();
                List<TaGzzTipoDestinatario> tiposDestinatarios = (new TaGzzTipoDestinatarioDao()).getAllActive();

                for (TaGzzTipoDestinatario tiposDestinatario : tiposDestinatarios) {
                    JsonObject recipientType = new JsonObject();
                    recipientType.addProperty("tipDstCod", tiposDestinatario.getTipDstCod());
                    recipientType.addProperty("tipDstDet", tiposDestinatario.getTipDstDet());
                    recipientTypes.add(recipientType);
                }

                data.add("tipDes", recipientTypes);

                JsonArray transportUnits = new JsonArray();
                List<EnP2mUnidadTransporte> unidadesTransporte = (new EnP2mUnidadTransporteDao()).getAllActive();

                for (EnP2mUnidadTransporte unidadTransporte : unidadesTransporte) {
                    JsonObject transportUnit = new JsonObject();
                    transportUnit.addProperty("uniTraCod", unidadTransporte.getUniTraCod());
                    transportUnit.addProperty("uniTraNumPla", unidadTransporte.getUniTraNumPla());
                    transportUnits.add(transportUnit);
                }

                data.add("numPla", transportUnits);

                JsonArray routs = new JsonArray();
                List<EnP1mClientesRutas> rutas = (new EnP1mClientesRutasDao()).getRutas4CliCod(bill.getEnP1mCliente().getCliCod());

                for (EnP1mClientesRutas ruta : rutas) {
                    JsonObject rout = new JsonObject();
                    rout.addProperty("rutCod", ruta.getEnP1mCatalogoRuta().getCatRutCod());
                    rout.addProperty("rutDet", ruta.getEnP1mCatalogoRuta().getCatRutDet());
                    routs.add(rout);
                }

                data.add("rutDes", routs);

                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(new Gson().toJson(data));
                break;
        }
    }

    private void addWhitOutRepeatClient(List<EnP1mCliente> l, EnP1mCliente cli) {
        for (EnP1mCliente dt : l) {
            if (cli.getCliCod().equals(dt.getCliCod())) {
                return;
            }
        }
        l.add(cli);
    }
}
