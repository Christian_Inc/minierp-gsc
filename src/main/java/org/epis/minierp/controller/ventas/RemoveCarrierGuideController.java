package org.epis.minierp.controller.ventas;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.epis.minierp.dao.logistica.LogEntGuiaTransportistaCabDao;
import org.epis.minierp.dao.ventas.EnP1mFacturaVentaCabDao;
import org.epis.minierp.model.EnP1mFacturaVentaCab;

public class RemoveCarrierGuideController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int facVenCabCod = Integer.parseInt(request.getParameter("facVenCabCod"));
        EnP1mFacturaVentaCabDao billDao = new EnP1mFacturaVentaCabDao();
        EnP1mFacturaVentaCab bill = billDao.getById(facVenCabCod);
        
        LogEntGuiaTransportistaCabDao guide = new LogEntGuiaTransportistaCabDao();

        bill.setEnP1mCatalogoRuta(null);
        billDao.update(bill);
    }
}
