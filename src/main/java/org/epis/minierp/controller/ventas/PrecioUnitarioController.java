package org.epis.minierp.controller.ventas;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.epis.minierp.dao.general.TaGzzListaPreciosDao;
import org.epis.minierp.dao.ventas.EnP2mPrecioUnitarioDao;
import org.epis.minierp.model.EnP2mPrecioUnitario;
import org.epis.minierp.model.EnP2mPrecioUnitarioId;
import org.epis.minierp.model.EnP2mProducto;
import org.epis.minierp.util.BigDecimalUtil;

public class PrecioUnitarioController extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private TaGzzListaPreciosDao lisPreDao;
    private EnP2mPrecioUnitarioDao preUniDao;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        lisPreDao = new TaGzzListaPreciosDao();
        request.setAttribute("listasPrecios", lisPreDao.getAllActive());
        request.setAttribute("productos", new ArrayList<EnP2mProducto>());

        request.getRequestDispatcher("/WEB-INF/ventas/precioUnitario/precioUnitario.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        lisPreDao = new TaGzzListaPreciosDao();
        preUniDao = new EnP2mPrecioUnitarioDao();

        int lisPreCod = Integer.parseInt(request.getParameter("lisPreCod"));
        String accion = request.getParameter("accion");

        request.setAttribute("listasPrecios", lisPreDao.getAllActive());

        EnP2mPrecioUnitario updatePrecio;
        String claProCod, subClaProCod, proCod;
        BigDecimal preUniCom, preUniMar, preUniFle, preUniVen;

        switch (accion) {
            case "load": {
                break;
            }
            case "update": {
                claProCod = request.getParameter("claProCod");
                subClaProCod = request.getParameter("subClaProCod");
                proCod = request.getParameter("proCod");

                preUniCom = new BigDecimal(request.getParameter("preUniCom"));
                preUniMar = new BigDecimal(request.getParameter("preUniMar"));
                preUniFle = new BigDecimal(request.getParameter("preUniFle"));

                preUniVen = BigDecimalUtil.sumar(preUniCom, preUniMar);
                preUniVen = BigDecimalUtil.sumar(preUniVen, preUniFle);

                updatePrecio = preUniDao.getById(new EnP2mPrecioUnitarioId(proCod, subClaProCod, claProCod, lisPreCod));
                updatePrecio.setPreUniCom(preUniCom);
                updatePrecio.setPreUniFle(preUniFle);
                updatePrecio.setPreUniMar(preUniMar);
                updatePrecio.setPreUniVen(preUniVen);

                preUniDao.update(updatePrecio);
                break;

            }
            default: {
            }
        }

        request.setAttribute("productos", preUniDao.getByListaPrecios(lisPreCod));
        request.getRequestDispatcher("/WEB-INF/ventas/precioUnitario/precioUnitario.jsp").forward(request, response);

    }
}
