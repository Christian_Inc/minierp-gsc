package org.epis.minierp.controller.general;

import java.io.File;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.epis.minierp.business.impresora.Impresora;
import org.epis.minierp.business.reportes.ReporterFactura;

public class ImpresionLotesController extends HttpServlet {

    private static final long serialVersionUID = 1L;
    ReporterFactura reporter;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/ventas/factura.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String realPath = request.getServletContext().getRealPath("");
        reporter = new ReporterFactura(realPath);

        String[] codigos = request.getParameterValues("codigos");
        String[] tipoDocumento = request.getParameterValues("tipos");

        int[] cods = new int[codigos.length];
        for (int i = 0; i < codigos.length; i++) {
            cods[i] = Integer.parseInt(codigos[i]);
        }
        String report = request.getParameter("report");

        String path = getServletContext().getRealPath("/WEB-INF/");
        path = path + "configuracion/impresora";
        Impresora generador = new Impresora(path);

        String fileGenerated = "";
        String printerName = "";
        switch (report) {
            case "factura":
//                for (int i = 0; i < cods.length; i++) {
//                    reporter.makeFacturaPdf(cods[i]);
//                }
                fileGenerated = generador.generateFacturas(cods)[0];
                printerName = generador.generateFacturas(cods)[1];
                break;
            case "boleta":
                fileGenerated = generador.generateBoletas(cods)[0];
                printerName = generador.generateBoletas(cods)[1];
                break;
            case "guiaRemision":
                fileGenerated = generador.generateGuiaRemision(cods)[0];
                printerName = generador.generateGuiaRemision(cods)[1];
                generador.sendToPrinter(new File(fileGenerated), printerName);
                response.sendRedirect(request.getContextPath() + "/secured/ventas/guiaRemisionTransporista");
                return;
        }
        generador.sendToPrinter(new File(fileGenerated), printerName);
        response.sendRedirect(request.getContextPath() + "/secured/ventas/factura");
    }
}
