package org.epis.minierp.controller.configuracion.usuario;

import java.io.IOException;

import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.epis.minierp.business.general.EnP1mUsuarioBusiness;
import org.epis.minierp.dao.general.EnP1mSucursalDao;
import org.epis.minierp.dao.general.EnP1mUsuarioDao;
import org.epis.minierp.dao.general.TaGzzCanalUsuarioDao;
import org.epis.minierp.dao.general.TaGzzEstadoCivilDao;
import org.epis.minierp.dao.general.TaGzzListaPreciosDao;
import org.epis.minierp.dao.general.TaGzzTipoDocUsuarioDao;
import org.epis.minierp.dao.general.TaGzzTipoUsuarioDao;
import org.epis.minierp.dao.general.TaGzzUnidadTrabajoDao;
import org.epis.minierp.dao.ventas.EnP1mCarteraClientesDao;
import org.epis.minierp.dao.ventas.EnP1mClienteDao;
import org.epis.minierp.dao.ventas.EnP1mDocumentoUsuarioDao;
import org.epis.minierp.dao.ventas.EnP1mListaPreciosUsuariosDao;
import org.epis.minierp.util.DateUtil;

public class UsuariosController extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    EnP1mUsuarioDao daoUsu;
    TaGzzEstadoCivilDao daoEstCiv;
    TaGzzTipoUsuarioDao tipUsuDao;
    EnP1mSucursalDao sucDao;
    EnP1mUsuarioBusiness usuarioBusiness;
    EnP1mCarteraClientesDao carCliDao;
    EnP1mDocumentoUsuarioDao docUsuDao;
    TaGzzTipoDocUsuarioDao tipDocUsu;
    EnP1mClienteDao cliDao;
    TaGzzCanalUsuarioDao canUsuDao;
    TaGzzListaPreciosDao lisPreDao;
    TaGzzUnidadTrabajoDao uniTraDao;
    EnP1mListaPreciosUsuariosDao lisPreUsuDao;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        daoUsu = new EnP1mUsuarioDao();
        daoEstCiv = new TaGzzEstadoCivilDao();
        tipUsuDao = new TaGzzTipoUsuarioDao();
        sucDao = new EnP1mSucursalDao();
        carCliDao = new EnP1mCarteraClientesDao();
        docUsuDao = new EnP1mDocumentoUsuarioDao();
        cliDao = new EnP1mClienteDao();
        tipDocUsu = new TaGzzTipoDocUsuarioDao();
        canUsuDao = new TaGzzCanalUsuarioDao();
        lisPreDao = new TaGzzListaPreciosDao();
        uniTraDao = new TaGzzUnidadTrabajoDao();
        lisPreUsuDao = new EnP1mListaPreciosUsuariosDao();
        
        Date hoy = DateUtil.getthisDate();
        String fecha = DateUtil.getString2Date(hoy);
        
        request.setAttribute("estados", daoEstCiv.getAllActive());                
        request.setAttribute("usuarios", daoUsu.getAllActive());
        request.setAttribute("inactivos",daoUsu.getAllInactive());
        request.setAttribute("tipos",tipUsuDao.getAllActive());
        request.setAttribute("sucursales",sucDao.getAllActive());
        request.setAttribute("carteraCli",carCliDao.getAllActive());
        request.setAttribute("clientes",cliDao.getAllActive());   
        request.setAttribute("documentos",tipDocUsu.getAllActive());
        request.setAttribute("documentosUsuarios",docUsuDao.getAllActive());
        request.setAttribute("canalesUsuarios",canUsuDao.getAllActive());
        request.setAttribute("unidadesTrabajo",uniTraDao.getAllActive());
        request.setAttribute("listasPrecios",lisPreDao.getAllActive());
        request.setAttribute("listaPreciosUsuarios",lisPreUsuDao.getAllActive());
        request.setAttribute("fecha",fecha);
        
        
        request.getRequestDispatcher("/WEB-INF/configuracion/usuario/usuarios.jsp").forward(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("accion");
        usuarioBusiness=new EnP1mUsuarioBusiness();
        
        String usuCod, usuNom, usuApePat, usuApeMat, usuLog, usuPas, docUsuNum, cliCod, usuCliDes, lisPreUsuDes, newPassword;
        int tipUsuCod, sucCod, tipDocUsuCod, estCivCod, canUsuCod, lisPreCod, uniTraCod;
        Date usuFecNac;
        char usuSex;
        
        switch(action){
            case "create":
                usuCod = request.getParameter("usuCod");
                usuNom = request.getParameter("usuNom");
                usuApePat = request.getParameter("usuApePat");
                usuApeMat = request.getParameter("usuApeMat");
                usuLog = request.getParameter("usuLog");
                usuPas = request.getParameter("usuPas");
                tipUsuCod = Integer.parseInt(request.getParameter("tipUsuCod"));
                sucCod = Integer.parseInt(request.getParameter("sucCod"));
                usuFecNac = DateUtil.getDate2String(request.getParameter("usuFecNac"));
                estCivCod = Integer.parseInt(request.getParameter("estCivCod"));
                canUsuCod = Integer.parseInt(request.getParameter("canUsuCod")); //canal
                usuSex = request.getParameter("usuSex").charAt(0);
                lisPreCod = Integer.parseInt(request.getParameter("lisPreCod"));
                uniTraCod = Integer.parseInt(request.getParameter("uniTraCod"));
                
                usuarioBusiness.create(usuCod, usuNom, usuApePat, usuApeMat, 
                        usuLog, usuPas,tipUsuCod, sucCod, usuFecNac, 
                        estCivCod, usuSex, canUsuCod,lisPreCod, uniTraCod);
                break;

            case "update":
                usuCod = request.getParameter("usuCod");
                usuNom = request.getParameter("usuNom");
                usuApePat= request.getParameter("usuApePat");
                usuApeMat = request.getParameter("usuApeMat");
                usuLog = request.getParameter("usuLog");
                tipUsuCod = Integer.parseInt(request.getParameter("tipUsuCod"));
                sucCod = Integer.parseInt(request.getParameter("sucCod"));
                usuFecNac = DateUtil.getDate2String(request.getParameter("usuFecNac"));
                estCivCod = Integer.parseInt(request.getParameter("estCivCod"));
                canUsuCod = Integer.parseInt(request.getParameter("canUsuCod")); //canal
                usuSex= request.getParameter("usuSex").charAt(0);
                lisPreCod = Integer.parseInt(request.getParameter("lisPreCod"));
                uniTraCod = Integer.parseInt(request.getParameter("uniTraCod"));
                
                usuarioBusiness.update(usuCod, usuNom, usuApePat, usuApeMat, 
                        usuLog, tipUsuCod, sucCod, usuFecNac,estCivCod, 
                        usuSex, canUsuCod,lisPreCod, uniTraCod);
                break;
                
            case "disable":{
                usuCod = request.getParameter("usuCod");
                usuarioBusiness.disable(usuCod);
                break;
                
            }
            case "activate":{
                usuCod = request.getParameter("usuCod");
                usuarioBusiness.activate(usuCod);
                break;
                
            }
            case "delete":{
                usuCod= request.getParameter("usuCod");
                usuarioBusiness.delete(usuCod);
                break;
            }
            
            case "documento":{
                usuCod = request.getParameter("usuCod");
                tipDocUsuCod = Integer.parseInt(request.getParameter("tipDocUsuCod"));
                docUsuNum = request.getParameter("docUsuNum");
                usuarioBusiness.saveOrUpdateDocumento(usuCod, tipDocUsuCod, docUsuNum, 'A');
                break;
            }
            
            case "cliente":{
                usuCod = request.getParameter("usuCod");
                cliCod =request.getParameter("cliCod");
                usuCliDes = request.getParameter("usuCliDes");
                usuarioBusiness.saveOrUpdateCarteraCli(usuCod, cliCod, usuCliDes, 'A');
                break;
            }
            
            case "listaPrecios":{
                usuCod = request.getParameter("usuCod");
                lisPreCod = Integer.parseInt(request.getParameter("lisPreCod"));
                lisPreUsuDes = request.getParameter("lisPreUsuDes");
                usuarioBusiness.saveOrUpdateListaPrecios(usuCod, lisPreCod, lisPreUsuDes, 'A');
                break;
            }
            
            case "modDocumento":{
                usuCod = request.getParameter("usuCod");
                tipDocUsuCod = Integer.parseInt(request.getParameter("tipDocUsuCod"));
                docUsuNum = request.getParameter("docUsuNum");
                usuarioBusiness.updateDocumento(usuCod, tipDocUsuCod, docUsuNum);
                break;
            }
            
            case "delDocumento":{
                usuCod = request.getParameter("usuCod");
                tipDocUsuCod = Integer.parseInt(request.getParameter("tipDocUsuCod"));
                usuarioBusiness.deleteDocumento(usuCod, tipDocUsuCod);
                break;
            }
            
            case "modCliente":{
                usuCod = request.getParameter("usuCod");
                cliCod =request.getParameter("cliCod");
                usuCliDes = request.getParameter("usuCliDes");
                usuarioBusiness.updateCarteraCli(usuCod, cliCod, usuCliDes);
                break;
            }
            
            case "delCliente":{
                usuCod = request.getParameter("usuCod");
                cliCod =request.getParameter("cliCod");
                usuarioBusiness.deleteCarteraCli(usuCod, cliCod);
                break;
            }
            
            case "modListaPrecios":{
                usuCod = request.getParameter("usuCod");
                lisPreCod = Integer.parseInt(request.getParameter("lisPreCod"));
                lisPreUsuDes = request.getParameter("lisPreUsuDes");
                usuarioBusiness.updateListaPrecios(usuCod, lisPreCod, lisPreUsuDes);
                break;
            }
            
            case "delListaPrecios":{
                usuCod = request.getParameter("usuCod");
                lisPreCod = Integer.parseInt(request.getParameter("lisPreCod"));
                usuarioBusiness.deleteListaPrecios(usuCod, lisPreCod);
                break;
            }
            
            case "updatePassword":{
                usuCod = request.getParameter("usuCod");
                newPassword = request.getParameter("newPassword");
                usuarioBusiness.changePassword(usuCod,newPassword);
                break;
            }
        }
        response.sendRedirect(request.getContextPath() + "/secured/configuracion/usuario/usuarios");
    }
}
