
package org.epis.minierp.dao.ventas;

import java.util.List;
import org.epis.minierp.model.EnP1mListaPreciosUsuarios;
import org.epis.minierp.model.EnP1mListaPreciosUsuariosId;
import org.epis.minierp.util.HibernateUtil;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Query;
import org.hibernate.Session;


public class EnP1mListaPreciosUsuariosDao {
    
    private Session session;

    public EnP1mListaPreciosUsuariosDao() {
        session = HibernateUtil.getSessionFactory().getCurrentSession();
    }

    public List<EnP1mListaPreciosUsuarios> getAll() {
        Query query = session.createQuery("from EnP1mListaPreciosUsuarios");
        List<EnP1mListaPreciosUsuarios> estados = query.list();
        return estados;
    }

    public List<EnP1mListaPreciosUsuarios> getAllActive() {
        Query query = session.createQuery("from EnP1mListaPreciosUsuarios E where E.estRegCod = 'A'");
        List<EnP1mListaPreciosUsuarios> estados = query.list();
        return estados;
    }
    
    public List<EnP1mListaPreciosUsuarios> getAllInactive() {
        Query query = session.createQuery("from EnP1mListaPreciosUsuarios E where E.estRegCod = 'I'");
        List<EnP1mListaPreciosUsuarios> estados = query.list();
        return estados;
    }

    public EnP1mListaPreciosUsuarios getById(EnP1mListaPreciosUsuariosId id) {
        EnP1mListaPreciosUsuarios estado = null;
        try {
            estado = (EnP1mListaPreciosUsuarios) session.load(EnP1mListaPreciosUsuarios.class, id);
        } catch (ObjectNotFoundException e) {
            return null;
        }
        return estado;
    }
    
    public List<EnP1mListaPreciosUsuarios> getAllActiveOrdered() {
        Query query = session.createQuery("from EnP1mListaPreciosUsuarios E where E.estRegCod = 'A' "
                + "order by E.id.lisPreCod, E.id.usuCod");
        List<EnP1mListaPreciosUsuarios> estados = query.list();
        return estados;
    }
    
    public void save(EnP1mListaPreciosUsuarios lisPreUsuario) {
        session.save(lisPreUsuario);     
    }
    
    public void update(EnP1mListaPreciosUsuarios lisPreUsuario){
        session.update(lisPreUsuario);
    }
    
    public void saveOrUpdate(EnP1mListaPreciosUsuarios lisPreUsuario){
        session.saveOrUpdate(lisPreUsuario);
    }
    
    public void delete(EnP1mListaPreciosUsuarios lisPreUsuario){
        session.delete(lisPreUsuario);
    }
}
