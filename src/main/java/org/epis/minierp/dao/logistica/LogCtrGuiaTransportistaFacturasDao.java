package org.epis.minierp.dao.logistica;

import java.util.List;
import org.epis.minierp.model.LogCtrGuiaTransportistaFacturas;
import org.epis.minierp.model.LogCtrGuiaTransportistaFacturasId;
import org.epis.minierp.util.HibernateUtil;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Query;
import org.hibernate.Session;

public class LogCtrGuiaTransportistaFacturasDao {

    private Session session;

    public LogCtrGuiaTransportistaFacturasDao() {
        session = HibernateUtil.getSessionFactory().getCurrentSession();
    }

    public List<LogCtrGuiaTransportistaFacturas> getAll() {
        Query query = session.createQuery("from LogCtrGuiaTransportistaFacturas");
        List<LogCtrGuiaTransportistaFacturas> estados = query.list();
        return estados;
    }

    public List<LogCtrGuiaTransportistaFacturas> getAllActive() {
        Query query = session.createQuery("from LogCtrGuiaTransportistaFacturas E where E.estReg = 'A'");
        List<LogCtrGuiaTransportistaFacturas> estados = query.list();
        return estados;
    }

    public List<LogCtrGuiaTransportistaFacturas> getAllInactive() {
        Query query = session.createQuery("from LogCtrGuiaTransportistaFacturas E where E.estReg = 'I'");
        List<LogCtrGuiaTransportistaFacturas> estados = query.list();
        return estados;
    }

    public LogCtrGuiaTransportistaFacturas getById(LogCtrGuiaTransportistaFacturasId id) {
        LogCtrGuiaTransportistaFacturas estado = null;
        try {
            estado = (LogCtrGuiaTransportistaFacturas) session.load(LogCtrGuiaTransportistaFacturas.class, id);
        } catch (ObjectNotFoundException e) {
            return null;
        }
        return estado;
    }

    public List<LogCtrGuiaTransportistaFacturas> getByFacVenCabCod(int facVenCabCod) {
        Query query = session.createQuery("from LogCtrGuiaTransportistaFacturas E where E.id.facVenCabCod = :hqlFacVenCabCod");
        query.setInteger("hqlFacVenCabCod", facVenCabCod);
        List<LogCtrGuiaTransportistaFacturas> estados = query.list();
        return estados;
    }

    public void save(LogCtrGuiaTransportistaFacturas guia) {
        session.save(guia);
    }

    public void update(LogCtrGuiaTransportistaFacturas guia) {
        session.update(guia);
    }

    public void saveOrUpdate(LogCtrGuiaTransportistaFacturas guia) {
        session.saveOrUpdate(guia);
    }

    public void delete(LogCtrGuiaTransportistaFacturas guia) {
        session.delete(guia);
    }
}
