
package org.epis.minierp.dao.logistica;

import java.util.List;
import org.epis.minierp.model.LogCtrGuiaTransportistaDet;
import org.epis.minierp.model.LogCtrGuiaTransportistaDetId;
import org.epis.minierp.util.HibernateUtil;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Query;
import org.hibernate.Session;

public class LogCtrGuiaTransportistaDetDao {
    private Session session;

    public LogCtrGuiaTransportistaDetDao() {
        session = HibernateUtil.getSessionFactory().getCurrentSession();
    }
    
    public List<LogCtrGuiaTransportistaDet> getAll() {
        Query query = session.createQuery("from LogCtrGuiaTransportistaDet");
        List<LogCtrGuiaTransportistaDet> estados = query.list();
        return estados;
    }

    public List<LogCtrGuiaTransportistaDet> getAllActive() {
        Query query = session.createQuery("from LogCtrGuiaTransportistaDet E where E.estReg = 'A'");
        List<LogCtrGuiaTransportistaDet> estados = query.list();
        return estados;
    }
    
    public List<LogCtrGuiaTransportistaDet> getAllInactive() {
        Query query = session.createQuery("from LogCtrGuiaTransportistaDet E where E.estReg = 'I'");
        List<LogCtrGuiaTransportistaDet> estados = query.list();
        return estados;
    }

    public LogCtrGuiaTransportistaDet getById(LogCtrGuiaTransportistaDetId id) {
        LogCtrGuiaTransportistaDet estado = null;
        try {
            estado = (LogCtrGuiaTransportistaDet) session.load(LogCtrGuiaTransportistaDet.class, id);
        } catch (ObjectNotFoundException e) {
            return null;
        }
        return estado;
    }
    
    public void save(LogCtrGuiaTransportistaDet guia) {
        session.save(guia);     
    }
    
    public void update(LogCtrGuiaTransportistaDet guia){
        session.update(guia);
    }
    
    public void saveOrUpdate(LogCtrGuiaTransportistaDet guia){
        session.saveOrUpdate(guia);
    }
    
    public void delete(LogCtrGuiaTransportistaDet guia){
        session.delete(guia);
    }
}
