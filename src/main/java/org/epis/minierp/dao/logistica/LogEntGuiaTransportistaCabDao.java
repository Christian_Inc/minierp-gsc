
package org.epis.minierp.dao.logistica;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.epis.minierp.model.LogEntGuiaTransportistaCab;
import org.epis.minierp.util.HibernateUtil;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Query;
import org.hibernate.Session;


public class LogEntGuiaTransportistaCabDao {
    
    private Session session;

    public LogEntGuiaTransportistaCabDao() {
        session = HibernateUtil.getSessionFactory().getCurrentSession();
    }
    
    public List<LogEntGuiaTransportistaCab> getAll() {
        Query query = session.createQuery("from LogEntGuiaTransportistaCab");
        List<LogEntGuiaTransportistaCab> estados = query.list();
        return estados;
    }

    public List<LogEntGuiaTransportistaCab> getAllActive() {
        Query query = session.createQuery("from LogEntGuiaTransportistaCab E where E.estReg = 'A'");
        List<LogEntGuiaTransportistaCab> estados = query.list();
        return estados;
    }
    
    public List<LogEntGuiaTransportistaCab> getAllInactive() {
        Query query = session.createQuery("from LogEntGuiaTransportistaCab E where E.estReg = 'I'");
        List<LogEntGuiaTransportistaCab> estados = query.list();
        return estados;
    }

    public LogEntGuiaTransportistaCab getById(int id) {
        LogEntGuiaTransportistaCab estado = null;
        try {
            estado = (LogEntGuiaTransportistaCab) session.load(LogEntGuiaTransportistaCab.class, id);
        } catch (ObjectNotFoundException e) {
            return null;
        }
        return estado;
    }
    
    public int getMaxGuiTraCabId() {
        Query query = session.createQuery("from LogEntGuiaTransportistaCab");
        List<LogEntGuiaTransportistaCab> estados = query.list();
        try {
            Set<Integer> lista = new HashSet<>();
            int temp;
            for (int i = 0; i < estados.size(); i++) {
                temp = estados.get(i).getGuiTraCabId();
                lista.add(temp);
            }
            return Collections.max(lista);
        } catch (Exception e) {
            return 0;
        }
    }
    
    public void save(LogEntGuiaTransportistaCab guia) {
        session.save(guia);     
    }
    
    public void update(LogEntGuiaTransportistaCab guia){
        session.update(guia);
    }
    
    public void saveOrUpdate(LogEntGuiaTransportistaCab guia){
        session.saveOrUpdate(guia);
    }
    
    public void delete(LogEntGuiaTransportistaCab guia){
        session.delete(guia);
    }
}
