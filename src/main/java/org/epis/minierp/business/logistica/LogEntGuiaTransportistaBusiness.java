package org.epis.minierp.business.logistica;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.epis.minierp.dao.compras.EnP4mFacturaCompraCabDao;
import org.epis.minierp.dao.logistica.LogCtrGuiaTransportistaDetDao;
import org.epis.minierp.dao.logistica.LogCtrGuiaTransportistaFacturasDao;
import org.epis.minierp.dao.logistica.LogEntGuiaTransportistaCabDao;
import org.epis.minierp.dao.ventas.EnP1mFacturaVentaCabDao;
import org.epis.minierp.model.EnP1mCatalogoRuta;
import org.epis.minierp.model.EnP1mEmpresa;
import org.epis.minierp.model.EnP1mFacturaVentaCab;
import org.epis.minierp.model.EnP1mUsuario;
import org.epis.minierp.model.EnP1tFacturaVentaDet;
import org.epis.minierp.model.EnP2mProducto;
import org.epis.minierp.model.EnP2mProductoId;
import org.epis.minierp.model.EnP2mTransportista;
import org.epis.minierp.model.EnP2mUnidadTransporte;
import org.epis.minierp.model.LogCtrGuiaTransportistaDet;
import org.epis.minierp.model.LogCtrGuiaTransportistaDetId;
import org.epis.minierp.model.LogCtrGuiaTransportistaFacturas;
import org.epis.minierp.model.LogCtrGuiaTransportistaFacturasId;
import org.epis.minierp.model.LogEntGuiaTransportistaCab;
import org.epis.minierp.model.TaGzzMotivoTraslado;
import org.epis.minierp.util.BigDecimalUtil;

public class LogEntGuiaTransportistaBusiness {

    LogEntGuiaTransportistaCabDao guiTraCabDao;
    EnP1mFacturaVentaCabDao facVenCabDao;
    EnP4mFacturaCompraCabDao facCabComDao;
    LogCtrGuiaTransportistaDetDao guiTraDetDao;
    LogCtrGuiaTransportistaFacturasDao guiTraFacDao;

    public LogEntGuiaTransportistaBusiness() {
        guiTraCabDao = new LogEntGuiaTransportistaCabDao();
        facVenCabDao = new EnP1mFacturaVentaCabDao();
        facCabComDao = new EnP4mFacturaCompraCabDao();
        guiTraDetDao = new LogCtrGuiaTransportistaDetDao();
        guiTraFacDao = new LogCtrGuiaTransportistaFacturasDao();
    }

    private void createGuiTraCab(int guiTraCabId, String guiTraCabNum, Date guiTraCabFecEmi,
            Date guiTraCabFecVen, int empCod, String usuCod, String traCod, String uniTraCod,
            int motTraCod, int catRutCod, char estReg) {

        EnP1mEmpresa emp = new EnP1mEmpresa();
        emp.setEmpCod(empCod);

        EnP1mUsuario usu = new EnP1mUsuario();
        usu.setUsuCod(usuCod);

        EnP2mTransportista tra = new EnP2mTransportista();
        tra.setTraCod(traCod);

        EnP2mUnidadTransporte uniTra = new EnP2mUnidadTransporte();
        uniTra.setUniTraCod(uniTraCod);

        TaGzzMotivoTraslado motTra = new TaGzzMotivoTraslado();
        motTra.setMotTraCod(motTraCod);
        
        EnP1mCatalogoRuta catRut = new EnP1mCatalogoRuta();
        catRut.setCatRutCod(catRutCod);

        LogEntGuiaTransportistaCab guiaRemTra = new LogEntGuiaTransportistaCab();
        guiaRemTra.setEnP1mCatalogoRuta(catRut);
        guiaRemTra.setEnP1mEmpresa(emp);
        guiaRemTra.setEnP1mUsuario(usu);
        guiaRemTra.setEnP2mTransportista(tra);
        guiaRemTra.setEnP2mUnidadTransporte(uniTra);
        guiaRemTra.setTaGzzMotivoTraslado(motTra);
        guiaRemTra.setGuiTraCabId(guiTraCabId);
        guiaRemTra.setGuiTraCabNum(guiTraCabNum);
        guiaRemTra.setGuiTraCabFecEmi(guiTraCabFecEmi);
        guiaRemTra.setGuiTraCabFecVen(guiTraCabFecVen);
        guiaRemTra.setEstReg(estReg);

        guiTraCabDao.save(guiaRemTra);

    }

    private void createGuiTraDet(int guiTraCabId, int guiTraDetId,
            String claProCod, String subClaProCod, String proCod,
            BigDecimal guiTraDetNumItm, Character estReg) {

        LogCtrGuiaTransportistaDetId gtdId = new LogCtrGuiaTransportistaDetId();
        gtdId.setGuiTraCabId(guiTraCabId);
        gtdId.setGuiTraDetId(guiTraDetId);

        EnP2mProductoId pId = new EnP2mProductoId();
        pId.setClaProCod(claProCod);
        pId.setSubClaProCod(subClaProCod);
        pId.setProCod(proCod);

        EnP2mProducto pro = new EnP2mProducto();
        pro.setId(pId);

        LogCtrGuiaTransportistaDet guiTraDet = new LogCtrGuiaTransportistaDet();
        guiTraDet.setEnP2mProducto(pro);
        guiTraDet.setId(gtdId);
        guiTraDet.setGuiTraDetNumItm(guiTraDetNumItm);
        guiTraDet.setEstReg(estReg);

        guiTraDetDao.save(guiTraDet);
    }

    /**
     * 
     * @param facVenCabCods
     * @param guiTraCabNumIni
     * @param guiTraCabFecEmi
     * @param guiTraCabFecVen
     * @param empCod
     * @param usuCod
     * @param traCod
     * @param uniTraCod
     * @param motTraCod
     * @param numMaxGuiTraDets
     * @return Retorna el numero de Guias de Remision Generadas
     */
    public int create4Ventas(int[] facVenCabCods, String guiTraCabNumIni,
            Date guiTraCabFecEmi, Date guiTraCabFecVen, int empCod,
            String usuCod, String traCod, String uniTraCod, int motTraCod,
            int catRutCod, int numMaxGuiTraDets) {

        List<LogCtrGuiaTransportistaDet> myDetalles = generateDets(facVenCabCods);

        //se calcula el numero de guias totales
        int size = myDetalles.size(); //cantidad de detalles insertados;
        int numGuiTraCab = size / numMaxGuiTraDets;
        if (size % numMaxGuiTraDets > 0 || size < numMaxGuiTraDets) {
            numGuiTraCab++;
        }

        String guiTraCabNum;
        int guiTraCabId;
        int tempNumDet = 0;
        LogCtrGuiaTransportistaDet tempGuiTraDet;

        String claProCod, subClaProCod, proCod;
        BigDecimal guiTraDetNumItm;

        for (int i = 0; i < numGuiTraCab; i++) {
            guiTraCabNum = GenerateGuiTraCabNum(guiTraCabNumIni, i);
            guiTraCabId = guiTraCabDao.getMaxGuiTraCabId() + 1;

            createGuiTraCab(guiTraCabId, guiTraCabNum, guiTraCabFecEmi, guiTraCabFecVen,
                    empCod, usuCod, traCod, uniTraCod, motTraCod, catRutCod, 'A');

            for (int j = tempNumDet; j < size && tempNumDet < numMaxGuiTraDets; j++) {
                tempGuiTraDet = myDetalles.get(j);
                claProCod = tempGuiTraDet.getEnP2mProducto().getId().getClaProCod();
                subClaProCod = tempGuiTraDet.getEnP2mProducto().getId().getSubClaProCod();
                proCod = tempGuiTraDet.getEnP2mProducto().getId().getProCod();
                guiTraDetNumItm = tempGuiTraDet.getGuiTraDetNumItm();

                //creando detalle de Guia de Transportista
                tempNumDet++; //inicia en 1
                createGuiTraDet(guiTraCabId, tempNumDet, claProCod, subClaProCod, proCod, guiTraDetNumItm, 'A');
            }
            tempNumDet = 0;
            
            //Agrupando las facturas a la guia Asociada
            GenerateGuiaTransportistaVentas(facVenCabCods, guiTraCabId);
        }
        
        return numGuiTraCab;
    }

    private List<LogCtrGuiaTransportistaDet> generateDets(int[] facVenCabCods) {

        List<LogCtrGuiaTransportistaDet> result = new ArrayList<>();
        List<EnP1tFacturaVentaDet> detalles = new ArrayList<>();
        boolean flag = false;
        int index = -1;
        BigDecimal oldValue;
        BigDecimal newValue;

        EnP1mFacturaVentaCab tempFacVenCab;
        for (int facVenCabCod : facVenCabCods) {
            tempFacVenCab = facVenCabDao.getById(facVenCabCod);
            detalles.addAll(tempFacVenCab.getEnP1tFacturaVentaDets());

            for (EnP1tFacturaVentaDet det : detalles) {
                for (int i = 0; i < result.size(); i++) {
                    if (det.getEnP2mProducto().equals(result.get(i).getEnP2mProducto())) {
                        flag = true;
                        index = i;
                        break;
                    }
                }
                if (flag) {
                    oldValue = result.get(index).getGuiTraDetNumItm();
                    newValue = BigDecimalUtil.sumar(oldValue, det.getFacVenDetCan());
                    result.get(index).setGuiTraDetNumItm(newValue);
                } else {
                    //se almacenan sin Id debido a que esta se asigna en el create del mismo
                    LogCtrGuiaTransportistaDet tempDet = new LogCtrGuiaTransportistaDet();
                    tempDet.setEnP2mProducto(det.getEnP2mProducto());
                    tempDet.setEstReg('A');
                    tempDet.setGuiTraDetNumItm(det.getFacVenDetCan());
                    result.add(tempDet);
                }
                flag = false;
                index = -1;
            }
            detalles.clear();
        }
        return result;
    }

    private void setEstReg(int guiTraCabId, char estReg) {
        LogEntGuiaTransportistaCab guiTraCab = guiTraCabDao.getById(guiTraCabId);
        guiTraCab.setEstReg(estReg);
        guiTraCabDao.update(guiTraCab);
    }

    public void activate(int guiTraCabId) {
        setEstReg(guiTraCabId, 'A');
    }

    public void disable(int guiTraCabId) {
        setEstReg(guiTraCabId, 'I');
    }

    public void delete(int guiTraCabId) {
        setEstReg(guiTraCabId, '*');
    }

    private String GenerateGuiTraCabNum(String guiTraCabNum, int add) {
        int last6digits = Integer.parseInt(guiTraCabNum.substring(4));
        last6digits = last6digits + add;
        String lote = guiTraCabNum.substring(0, 4); //lote xxx-
        String code = String.format("%06d", last6digits); //formato de 6 digitos con 0s a la izquierda
        return lote + code;
    }

    private void GenerateGuiaTransportistaVentas(int[] facVenCabCods, int guiTraCabId) {
        for (int i = 0; i < facVenCabCods.length; i++) {
            LogCtrGuiaTransportistaFacturas tempGtf = new LogCtrGuiaTransportistaFacturas();
            tempGtf.setId(new LogCtrGuiaTransportistaFacturasId(facVenCabCods[i], guiTraCabId));
            tempGtf.setEstReg('A');
            guiTraFacDao.save(tempGtf);
        }

    }

}
