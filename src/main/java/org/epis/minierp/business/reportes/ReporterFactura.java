package org.epis.minierp.business.reportes;

import com.itextpdf.kernel.color.Color;
import com.itextpdf.layout.Style;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.StringTokenizer;
import org.epis.minierp.dao.general.EnP1mEmpresaDao;
import org.epis.minierp.dao.ventas.EnP1mFacturaVentaCabDao;
import org.epis.minierp.model.EnP1mFacturaVentaCab;
import org.epis.minierp.model.EnP2mGuiaRemRemitente;
import org.epis.minierp.model.LogEntGuiaTransportistaCab;
import org.epis.minierp.util.BigDecimalUtil;
import org.epis.minierp.util.reporter.ITextReport;
import org.epis.minierp.util.reporter.ITextToken;
import org.epis.minierp.util.reporter.ITextUtil;

public class ReporterFactura {

    private EnP1mFacturaVentaCabDao facVenCabDao;
    private EnP1mEmpresaDao empDao;

    private final String realPath;
    private final String templateFactura;
    private final String templateBoleta;
    private final String templateGuiaRTransportista;
    private final String templateGuiaRRemitente;
    private final String facturaPath;
    private final String boletaPath;
    private final String guiaRTransportistaPath;
    private final String guiaRRemitentePath;
    private final Style myFontMonoSpaced;

    private EnP1mFacturaVentaCab myFactura;
    private EnP1mFacturaVentaCab myBoleta;
    private LogEntGuiaTransportistaCab myGuiaRTransportista;
    private EnP2mGuiaRemRemitente myGuiaRRemitente;

    private final DateFormat DATE_FORMAT_TIME = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
    private final DateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
    private final String EXT = ".pdf";

    private final int numDecimales;

    public ReporterFactura(String realPath) {
        facVenCabDao = new EnP1mFacturaVentaCabDao();
        empDao = new EnP1mEmpresaDao();
        numDecimales = empDao.getById(01).getEmpNumDec();
        this.realPath = realPath;

        this.myFontMonoSpaced = ITextUtil.getFont(realPath + "resources\\fonts\\lekton\\Lekton-Regular.ttf", 9, Color.BLACK, false, false, false);

        this.templateFactura = realPath + "resources\\templates\\factura.fac";
        this.templateBoleta = realPath + "resources\\templates\\boleta.bol";
        this.templateGuiaRTransportista = realPath + "resources\\templates\\guiaRemisionRemitente.grt";
        this.templateGuiaRRemitente = realPath + "resources\\templates\\guiaRemisionTransportista.grr";
        this.facturaPath = realPath + "reports\\facturas\\";
        this.boletaPath = realPath + "reports\\boletas\\";
        this.guiaRTransportistaPath = realPath + "reports\\guiaRTransportista\\";
        this.guiaRRemitentePath = realPath + "reports\\guiaRRemitente\\";
    }

    public boolean makeFacturaPdf(int facVenCabCod) {
        myFactura = facVenCabDao.getById(facVenCabCod);
        ITextReport myReport = getTokens(templateFactura);
        myReport.close();
        return myReport != null;
    }

    /**
     * Funcion que genera
     *
     * @param url de la plantilla de la impresion de factura con formato
     * adecuado
     * @return retorna el Reporte
     */
    public ITextReport getTokens(String url) {
        String ext = url.substring(url.length() - 4);

        if (ext.equals(".fac") || ext.equals(".bol")
                || ext.equals(".grr") || ext.equals(".grt")) {
            try {
                File casesSet = new File(url);
                FileReader fr = new FileReader(casesSet);
                BufferedReader br = new BufferedReader(fr);
                ArrayList<String> lines = new ArrayList<>();

                String tempLine;
                while ((tempLine = br.readLine()) != null) {
                    tempLine = tempLine.trim();
                    if (tempLine.length() == 0) {
                        continue;
                    }
                    if (!tempLine.substring(0, 2).equals("//")) {
                        lines.add(tempLine);
                    }
                }
                br.close();
                switch (ext) {
                    case ".fac": {
                        return readPlantillaFactura(lines);
                    }
                    case ".bol": {
                        return readPlantillaBoleta(lines);
                    }
                    case ".grr": {
                        return readPlantillaGuiRTransportista(lines);
                    }
                    case ".grt": {
                        return readPlantillaGuiaRRemitente(lines);
                    }
                    default: {
                        return null;
                    }
                }
            } catch (FileNotFoundException ex) {
                return null;
            } catch (IOException ex) {
                return null;
            }
        } else {
            return null;
        }
    }

    public ITextReport readPlantillaFactura(ArrayList<String> lines) {
        ITextReport report;
        String name = "factura_" + myFactura.getFacVenCabCod() + "_"
                + myFactura.getFacVenCabNum() + EXT;
        report = getITextReport(lines.get(0), facturaPath, name);

        for (int i = 1; i < lines.size(); i++) {
            readLineFactura(lines.get(i), report);
        }
        return report;
    }

    public ITextReport readPlantillaBoleta(ArrayList<String> lines) {
        return null;
    }

    public ITextReport readPlantillaGuiRTransportista(ArrayList<String> lines) {
        return null;
    }

    public ITextReport readPlantillaGuiaRRemitente(ArrayList<String> lines) {
        return null;
    }

    private ITextReport getITextReport(String configLine, String path, String name) {
        StringTokenizer stConfig = new StringTokenizer(configLine, "|");
        ArrayList<String> config = new ArrayList<>();
        String[] pageToken;
        double width = 21.0;
        double height = 29.7;
        double leftMargin = 2.0;
        double rightMargin = 2.0;
        double topMargin = 2.0;
        double bottonMargin = 2.0;

        while (stConfig.hasMoreTokens()) {
            config.add(stConfig.nextToken().trim());
        }
        if (config.get(0).equals("P:")) {
            try {
                for (int i = 1; i < config.size(); i++) {
                    pageToken = getPageToken(config.get(i));
                    switch (pageToken[0]) {
                        case "W": {
                            width = Double.parseDouble(pageToken[1]);
                            break;
                        }
                        case "H": {
                            height = Double.parseDouble(pageToken[1]);
                            break;
                        }
                        case "LM": {
                            leftMargin = Double.parseDouble(pageToken[1]);
                            break;
                        }
                        case "RM": {
                            rightMargin = Double.parseDouble(pageToken[1]);
                            break;
                        }
                        case "TM": {
                            topMargin = Double.parseDouble(pageToken[1]);
                            break;
                        }
                        case "BM": {
                            bottonMargin = Double.parseDouble(pageToken[1]);
                            break;
                        }
                        case "SL": {
                            myFontMonoSpaced.setFontSize(Float.parseFloat(pageToken[1]));
                            break;
                        }
                    }
                }
            } catch (Exception e) {
            }
        }
        return new ITextReport(path, name, width, height,
                leftMargin, rightMargin, topMargin, bottonMargin);
    }

    private String[] getPageToken(String token) {
        String[] configToken = new String[2];
        boolean flag = false;
        for (int i = 1; i < token.length(); i++) {
            if (token.charAt(i) == ':') {
                if (token.charAt(i + 1) == '=') {
                    flag = true;
                    configToken[0] = token.substring(0, i).trim();
                    configToken[1] = token.substring(i + 2, token.length()).trim();
                    break;
                }
            }
        }
        if (flag) {
            return configToken;
        } else {
            configToken[0] = "NN";
            configToken[1] = "0.0";
            return configToken;
        }
    }

    private void readLineFactura(String line, ITextReport report) {
        StringTokenizer stLine = new StringTokenizer(line, "|");
        ArrayList<String> tokens = new ArrayList<>();
        ArrayList<ITextToken> temp = new ArrayList<>();
        while (stLine.hasMoreTokens()) {
            tokens.add(stLine.nextToken().trim());
        }
        if (tokens.get(0).equals("L:")) {
            for (int i = 1; i < tokens.size(); i++) {
                temp.add(getFacturaLineToken(tokens.get(i)));
            }
            report.addLine(temp, myFontMonoSpaced);
            return;
        }
        if (tokens.get(0).equals("D:")) {
            ArrayList<ArrayList<ITextToken>> details;
            details = getFacturaDetailTokens(tokens);
            for (ArrayList<ITextToken> detail : details) {
                report.addLine(detail, myFontMonoSpaced);
            }
        }
    }

    private ITextToken getFacturaLineToken(String token) {
        ITextToken lineToken = new ITextToken();
        int size = 0;
        String content = "";
        for (int i = 1; i < token.length(); i++) {
            if (token.charAt(i) == ':') {
                if (token.charAt(i + 1) == '=') {
                    content = readFacturaToken(token.substring(0, i).trim());
                    size = Integer.parseInt(token.substring(i + 2, token.length()).trim());
                    break;
                }
            }
        }
        lineToken.setContent(content);
        lineToken.setSize(size);
        return lineToken;
    }

    /**
     * Tanto Ruta como GuiRRemitente estan en desuso
     *
     * @param token
     * @return
     */
    private String readFacturaToken(String token) {
        char sign = token.charAt(0);
        String key = token.substring(1, token.length()).trim();
        switch (sign) {
            case '$': {
                switch (key) {
                    case "space": {
                        return "";
                    }
                    //Cliente
                    case "cliCod": {
                        return myFactura.getEnP1mCliente().getCliCod();
                    }
                    case "cliRazSoc": {
                        return myFactura.getEnP1mCliente().getCliRazSoc();
                    }
                    case "cliNomCom": {
                        return myFactura.getEnP1mCliente().getCliNomCom();
                    }
                    case "cliDomFis": {
                        return myFactura.getEnP1mCliente().getCliDomFis();
                    }
                    case "cliNom": {
                        return myFactura.getEnP1mCliente().getCliNom();
                    }
                    case "cliApePat": {
                        return myFactura.getEnP1mCliente().getCliApePat();
                    }
                    case "cliApeMat": {
                        return myFactura.getEnP1mCliente().getCliApeMat();
                    }
                    case "cliNomyApe": {
                        return myFactura.getEnP1mCliente().getCliNom() + " "
                                + myFactura.getEnP1mCliente().getCliApePat() + " "
                                + myFactura.getEnP1mCliente().getCliApeMat();
                    }
                    case "cliSex": {
                        return "" + myFactura.getEnP1mCliente().getCliSex();
                    }
                    case "cliDir": {
                        return myFactura.getEnP1mCliente().getCliDir();
                    }
                    case "cliTelFij": {
                        return myFactura.getEnP1mCliente().getCliTelFij();
                    }
                    case "cliTelCel": {
                        return myFactura.getEnP1mCliente().getCliTelCel();
                    }
                    case "cliEmail": {
                        return myFactura.getEnP1mCliente().getCliEmail();
                    }
                    case "cliCanCod": {
                        return "" + myFactura.getEnP1mCliente().getTaGzzCanalCliente().getCanCliCod();
                    }
                    case "cliCanDet": {
                        return myFactura.getEnP1mCliente().getTaGzzCanalCliente().getCanCliDet();
                    }
                    case "cliEstCivCod": {
                        return "" + myFactura.getEnP1mCliente().getTaGzzEstadoCivil().getEstCivCod();
                    }
                    case "cliEstCivDet": {
                        return myFactura.getEnP1mCliente().getTaGzzEstadoCivil().getEstCivDet();
                    }
                    case "cliTipCod": {
                        return "" + myFactura.getEnP1mCliente().getTaGzzTipoCliente().getTipCliCod();
                    }
                    case "cliTipDet": {
                        return myFactura.getEnP1mCliente().getTaGzzTipoCliente().getTipCliDet();
                    }
                    //Usuario(vendedor)
                    case "usuCod": {
                        return myFactura.getEnP1mUsuario().getUsuCod();
                    }
                    case "usuNom": {
                        return myFactura.getEnP1mUsuario().getUsuNom();
                    }
                    case "usuApePat": {
                        return myFactura.getEnP1mUsuario().getUsuApePat();
                    }
                    case "usuApeMat": {
                        return myFactura.getEnP1mUsuario().getUsuApeMat();
                    }
                    case "usuNomyApe": {
                        return myFactura.getEnP1mUsuario().getUsuNom() + " "
                                + myFactura.getEnP1mUsuario().getUsuApePat() + " "
                                + myFactura.getEnP1mUsuario().getUsuApeMat();
                    }
                    case "usuLog": {
                        return "" + myFactura.getEnP1mUsuario().getUsuLog();
                    }
                    case "usuFecNac": {
                        return DATE_FORMAT.format(myFactura.getEnP1mUsuario().getUsuFecNac());
                    }
                    case "usuSex": {
                        return "" + myFactura.getEnP1mUsuario().getUsuSex();
                    }
                    case "usuSucCod": {
                        return "" + myFactura.getEnP1mUsuario().getEnP1mSucursal().getSucCod();
                    }
                    case "usuSucDet": {
                        return myFactura.getEnP1mUsuario().getEnP1mSucursal().getSucDes();
                    }
                    case "usuSucDir": {
                        return myFactura.getEnP1mUsuario().getEnP1mSucursal().getSucDir();
                    }
                    case "usuCanCod": {
                        return "" + myFactura.getEnP1mUsuario().getTaGzzCanalUsuario().getCanUsuCod();
                    }
                    case "usuCanDet": {
                        return myFactura.getEnP1mUsuario().getTaGzzCanalUsuario().getCanUsuDet();
                    }
                    case "usuCanPorAdd": {
                        return getCanUsuPorAdd(myFactura.getEnP1mUsuario().getTaGzzCanalUsuario().getCanUsuPorAdd());
                    }
                    case "usuEstCivCod": {
                        return "" + myFactura.getEnP1mUsuario().getTaGzzEstadoCivil().getEstCivCod();
                    }
                    case "usuEstCivDet": {
                        return myFactura.getEnP1mUsuario().getTaGzzEstadoCivil().getEstCivDet();
                    }
                    case "usuLisPreCod": {
                        return "" + myFactura.getEnP1mUsuario().getTaGzzListaPrecios().getLisPreCod();
                    }
                    case "usuLisPreDet": {
                        return myFactura.getEnP1mUsuario().getTaGzzListaPrecios().getLisPreDet();
                    }
                    case "usuTipCod": {
                        return "" + myFactura.getEnP1mUsuario().getTaGzzTipoUsuario().getTipUsuCod();
                    }
                    case "usuTipDet": {
                        return myFactura.getEnP1mUsuario().getTaGzzTipoUsuario().getTipUsuDet();
                    }
                    case "usuUniTraCod": {
                        return "" + myFactura.getEnP1mUsuario().getTaGzzUnidadTrabajo().getUniTraCod();
                    }
                    case "usuUniTraDet": {
                        return myFactura.getEnP1mUsuario().getTaGzzUnidadTrabajo().getUniTraDet();
                    }
                    //Estado Factura
                    case "estFacCod": {
                        return "" + myFactura.getTaGzzEstadoFactura().getEstFacCod();
                    }
                    case "estFacDet": {
                        return myFactura.getTaGzzEstadoFactura().getEstFacDet();
                    }
                    //Metodo Pago Factura
                    case "metPagCod": {
                        return "" + myFactura.getTaGzzMetodoPagoFactura().getMetPagCod();
                    }
                    case "metPagDet": {
                        return myFactura.getTaGzzMetodoPagoFactura().getMetPagDet();
                    }
                    //Moneda
                    case "monCod": {
                        return "" + myFactura.getTaGzzMoneda().getMonCod();
                    }
                    case "monDet": {
                        return myFactura.getTaGzzMoneda().getMonDet();
                    }
                    case "monSim": {
                        return myFactura.getTaGzzMoneda().getMonSim();
                    }
                    //Tipo Descuento
                    case "tipDesCod": {
                        return "" + myFactura.getTaGzzTipoDescuento().getTipDesCod();
                    }
                    case "tipDesDet": {
                        return myFactura.getTaGzzTipoDescuento().getTipDesDet();
                    }
                    //Tipo de Pago
                    case "tipPagCod": {
                        return "" + myFactura.getTaGzzTipoPagoFactura().getTipPagCod();
                    }
                    case "tipPagDet": {
                        return myFactura.getTaGzzTipoPagoFactura().getTipPagDet();
                    }
                    //Factura
                    case "facCod": {
                        return "" + myFactura.getFacVenCabCod();
                    }
                    case "facNum": {
                        return myFactura.getFacVenCabNum();
                    }
                    case "facModVenCod": {
                        return "" + myFactura.getFacVenCabModVen();
                    }
                    case "facModVenDet": {
                        return getFacVenCabModVen(myFactura.getFacVenCabModVen());
                    }
                    case "facFecEmi": {
                        return DATE_FORMAT.format(myFactura.getFacVenCabFecEmi());
                    }
                    case "facFecVen": {
                        return DATE_FORMAT.format(myFactura.getFacVenCabFecVen());
                    }
                    case "facPorDes": {
                        return "" + myFactura.getFacVenPorDes() + " %";
                    }
                    case "facIGV": {
                        return "" + myFactura.getFacVenCabIgv() + " %";
                    }
                    case "facValNet": {
                        return getBigDecimal(myFactura.getFacVenCabTot());
                    }
                    case "facValIGV": {
                        return getBigDecimal(myFactura.getFacVenCabSubTot());
                    }
                    case "facValTot": {
                        return getBigDecimalSuma(myFactura.getFacVenCabTot(), myFactura.getFacVenCabSubTot());
                    }
                    case "facObs": {
                        return myFactura.getFacVenCabObs();
                    }
                    default: {
                        return token;
                    }
                }
            }
            case '#': {
                if (key.charAt(0) == '(' && key.charAt(key.length() - 1) == ')') {
                    return key.substring(1, key.length() - 1);
                } else {
                    return token;
                }
            }
            default: {
                return token;
            }
        }
    }

    private ArrayList<ArrayList<ITextToken>> getFacturaDetailTokens(ArrayList<String> tokens) {
        return new ArrayList<>();
    }

    private String getCanUsuPorAdd(double porAdd) {
        double rpt = porAdd * 100;
        return "" + rpt + "%";
    }

    private String getFacVenCabModVen(char facVenCabModVen) {
        switch (facVenCabModVen) {
            case 'F':
                return "Factura";
            case 'B':
                return "Boleta";
            case 'N':
                return "Nota de Pedido";
            default:
                return "No Especificado";
        }
    }

    private String getBigDecimal(BigDecimal number) {
        return "" + BigDecimalUtil.get(number, numDecimales);
    }

    private String getBigDecimalSuma(BigDecimal number1, BigDecimal number2) {
        BigDecimal number = BigDecimalUtil.sumar(number1, number2);
        return "" + BigDecimalUtil.get(number, numDecimales);
    }
}
