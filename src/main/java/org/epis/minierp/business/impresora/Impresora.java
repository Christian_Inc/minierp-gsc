package org.epis.minierp.business.impresora;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.epis.minierp.dao.general.EnP1mEmpresaDao;
import org.epis.minierp.dao.logistica.EnP2mDocumentoTransportistaDao;
import org.epis.minierp.dao.logistica.LogCtrGuiaTransportistaFacturasDao;
import org.epis.minierp.dao.logistica.LogEntGuiaTransportistaCabDao;
import org.epis.minierp.dao.ventas.EnP1mClienteDao;
import org.epis.minierp.dao.ventas.EnP1mDocumentoClienteDao;
import org.epis.minierp.dao.ventas.EnP1mFacturaVentaCabDao;
import org.epis.minierp.model.EnP1mCliente;
import org.epis.minierp.model.EnP1mDocumentoClienteId;
import org.epis.minierp.model.EnP1mEmpresa;
import org.epis.minierp.model.EnP1mFacturaVentaCab;
import org.epis.minierp.model.EnP1tFacturaVentaDet;
import org.epis.minierp.model.EnP2mDocumentoTransportistaId;
import org.epis.minierp.model.EnP2mTransportista;
import org.epis.minierp.model.EnP2mUnidadTransporte;
import org.epis.minierp.model.LogCtrGuiaTransportistaDet;
import org.epis.minierp.model.LogCtrGuiaTransportistaFacturas;
import org.epis.minierp.model.LogEntGuiaTransportistaCab;
import org.epis.minierp.util.BigDecimalUtil;
import org.epis.minierp.util.DateUtil;
import static org.epis.minierp.util.NumberToLetterConverter.convertNumberToLetter;

public class Impresora {

    String extension = ".prn";
    String path;
    SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
    DateFormat fecha = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    DecimalFormat df = new DecimalFormat("0.00");

    EnP1mEmpresaDao empDao;
    EnP1mClienteDao cliDao;
    LogEntGuiaTransportistaCabDao guiTraCabDao;
    LogCtrGuiaTransportistaFacturasDao guiTraFacDao;

    EnP1mEmpresa myEmpresa;

    int tipCod = 1;
    String cliNom, cliDir, cliRuc, fecEmi;
    //Factura
    String cliCod, conPag, fecVen, venZon, numSec, dis, rut, traNom;
    String proCod;
    int proNum;
    String proUni, proDes, proDes1, proDes2;
    Double proCan, proValUni, proValNet;
    String totLet;
    Double neto, igv, total;
    //Boleta
    String venRut, pdv, obs;
    //Guia remisión
    String punPar, punLle;
    String traLic, traPla, cliRUC;
    String ven, zon, con, oc, facNum, hora, numInt, motTra;
    DateFormat converter;
    int empNumDec, empNumDetGuiTra;

    public Impresora(String path) {
        empDao = new EnP1mEmpresaDao();
        cliDao = new EnP1mClienteDao();
        guiTraCabDao = new LogEntGuiaTransportistaCabDao();
        guiTraFacDao = new LogCtrGuiaTransportistaFacturasDao();

        this.path = path;
        converter = new SimpleDateFormat("dd/MM/yyyy");
        converter.setTimeZone(TimeZone.getTimeZone("UTC"));

        myEmpresa = empDao.getById(01);
        empNumDetGuiTra = myEmpresa.getEmpNumDetGuiRemTra();
        empNumDec = myEmpresa.getEmpNumDec();
    }

    public String generateFactura(int cod) {
        empDao = new EnP1mEmpresaDao();
        EnP1mEmpresa e = empDao.getById(01);

        String file = "Factura_" + sf.format(date.getTime()) + extension;
        ImpresoraMatricial fac = new ImpresoraMatricial(file, path, "factura");
        BigDecimal temp;
        try {
            EnP1mFacturaVentaCab f = (new EnP1mFacturaVentaCabDao()).getById(cod);

            String myDateEmi = converter.format(f.getFacVenCabFecEmi());
            String myDateVen = converter.format(f.getFacVenCabFecVen());

            cliNom = f.getEnP1mCliente().getCliNom();
            cliDir = f.getEnP1mCliente().getCliDir();
            cliCod = f.getEnP1mCliente().getCliCod();

            try {
                String tempRUC = (new EnP1mDocumentoClienteDao()).getById(new EnP1mDocumentoClienteId(cliCod, 2)).getDocCliNum();
                if (!tempRUC.trim().equals("")) {
                    cliRuc = "RUC: " + tempRUC;
                } else {
                    cliRuc = "-";
                }
            } catch (Exception e1) {
                cliRuc = "-";
            }

            fecEmi = myDateEmi;
            fac.writeFacSobCab(cliNom, cliDir, cliRuc, fecEmi);

            conPag = f.getTaGzzMetodoPagoFactura().getMetPagDet();
            fecVen = myDateVen;
            venZon = f.getEnP1mUsuario().getUsuNom();
            numSec = " ";
            dis = " ";
            try {
                rut = Integer.toString(f.getEnP1mCatalogoRuta().getCatRutCod());
            } catch (Exception e2) {
                rut = "-";
            }
            traNom = "-";

            fac.writeFacCabecera(cliCod, conPag, fecVen, venZon, numSec, dis, rut, traNom);

            proNum = 0;
            List<EnP1tFacturaVentaDet> detalles = (new EnP1mFacturaVentaCabDao()).getFacVenDets(cod);
            for (EnP1tFacturaVentaDet d : detalles) {
                proNum++;
                proCod = d.getEnP2mProducto().getId().getProCod();
                proCan = BigDecimalUtil.get(d.getFacVenDetCan(), 0);
                proUni = d.getEnP2mProducto().getTaGzzUnidadMed().getUniMedSim();
                proDes = d.getEnP2mProducto().getProDet();
                proValUni = BigDecimalUtil.get(d.getFacVenDetValUni(), 4);
                proDes1 = Integer.toString(f.getFacVenPorDes()) + "%";
                proDes2 = "0%";
                temp = BigDecimalUtil.multiplicar(d.getFacVenDetCan(), d.getFacVenDetValUni(), empNumDec);
                proValNet = BigDecimalUtil.get(temp, 4);
                fac.writeFacDetalle(tipCod, proNum, proCod, proCan, proUni, proDes, proValUni, proDes1, proDes2, df.format(proValNet));
            }
            fac.addLines(e.getEmpNumDetFacVen() - proNum - 1);
            neto = BigDecimalUtil.get(f.getFacVenCabTot(), 4);
            igv = BigDecimalUtil.get(f.getFacVenCabSubTot(), 4);
            total = BigDecimalUtil.get(BigDecimalUtil.sumar(f.getFacVenCabTot(), f.getFacVenCabSubTot()), 4);
            totLet = convertNumberToLetter(total);
            fac.writeFacLetras(totLet);

            fac.writeFacTotal(df.format(neto), df.format(igv), df.format(total));
            fac.close();
        } catch (IOException ex) {
            Logger.getLogger(Impresora.class.getName()).log(Level.SEVERE, null, ex);
        }
        return file;
    }

    public String[] generateFacturas(int[] cods) {
        empDao = new EnP1mEmpresaDao();
        EnP1mEmpresa e = empDao.getById(01);
        String file = "Facturas_" + sf.format(date.getTime()) + extension;
        ImpresoraMatricial fac = new ImpresoraMatricial(file, path, "factura");
        BigDecimal temp;
        try {
            for (int cod : cods) {
                EnP1mFacturaVentaCab f = (new EnP1mFacturaVentaCabDao()).getById(cod);
                String myDateEmi = converter.format(f.getFacVenCabFecEmi());
                String myDateVen = converter.format(f.getFacVenCabFecVen());

                cliCod = f.getEnP1mCliente().getCliCod();
                try {
                    cliNom = f.getEnP1mCliente().getCliRazSoc();
                } catch (Exception fa) {
                    cliNom = "-";
                }
                try {
                    cliDir = f.getEnP1mCliente().getCliDir();
                } catch (Exception fa) {
                    cliDir = "-";
                }
                try {
                    String tempRUC = (new EnP1mDocumentoClienteDao()).getById(new EnP1mDocumentoClienteId(cliCod, 2)).getDocCliNum();
                    if (!tempRUC.trim().equals("")) {
                        cliRuc = "RUC: " + tempRUC;
                    } else {
                        cliRuc = "-";
                    }
                } catch (Exception fa) {
                    cliRuc = "-";
                }
                fecEmi = myDateEmi;
                fac.writeFacSobCab(cliNom, cliDir, cliRuc, fecEmi);

                conPag = f.getTaGzzMetodoPagoFactura().getMetPagDet();
                fecVen = myDateVen;
                venZon = f.getEnP1mUsuario().getUsuNom();
                numSec = " ";
                dis = " ";
                try {
                    rut = Integer.toString(f.getEnP1mCatalogoRuta().getCatRutCod());
                } catch (Exception e2) {
                    rut = "-";
                }
                traNom = "-";
                fac.writeFacCabecera(cliCod, conPag, fecVen, venZon, numSec, dis, rut, traNom);

                proNum = 0;
                List<EnP1tFacturaVentaDet> detalles = (new EnP1mFacturaVentaCabDao()).getFacVenDets(cod);
                for (EnP1tFacturaVentaDet d : detalles) {
                    proNum++;
                    proCod = d.getEnP2mProducto().getId().getProCod();
                    proCan = BigDecimalUtil.get(d.getFacVenDetCan(), 0);
                    proUni = d.getEnP2mProducto().getTaGzzUnidadMed().getUniMedSim();
                    proDes = d.getEnP2mProducto().getProDet();
                    proValUni = BigDecimalUtil.get(d.getFacVenDetValUni(), 4);
                    proDes1 = Integer.toString(f.getFacVenPorDes()) + "%";
                    proDes2 = "0%";
                    temp = BigDecimalUtil.multiplicar(d.getFacVenDetCan(), d.getFacVenDetValUni(), empNumDec);
                    proValNet = BigDecimalUtil.get(temp, 4);
                    fac.writeFacDetalle(tipCod, proNum, proCod, proCan, proUni, proDes, proValUni, proDes1, proDes2, df.format(proValNet));
                }
                fac.addLines(e.getEmpNumDetFacVen() - proNum - 1);
                neto = BigDecimalUtil.get(f.getFacVenCabTot(), 4);
                igv = BigDecimalUtil.get(f.getFacVenCabSubTot(), 4);
                total = BigDecimalUtil.get(BigDecimalUtil.sumar(f.getFacVenCabTot(), f.getFacVenCabSubTot()), 4);
                totLet = convertNumberToLetter(total);
                fac.writeFacLetras(totLet);
                fac.writeFacTotal(df.format(neto), df.format(igv), df.format(total));
                neto = 0.0;
                igv = 0.0;
                total = 0.0;
            }
            fac.close();
        } catch (IOException ex) {
        }
        String params[] = {file, fac.getName()};
        return params;
    }

    public String[] generateBoletas(int[] cods) {
        empDao = new EnP1mEmpresaDao();
        EnP1mEmpresa e = empDao.getById(01);
        String cliDni;
        String file = "Boletas_" + sf.format(date.getTime()) + extension;
        ImpresoraMatricial bol = new ImpresoraMatricial(file, path, "boleta");
        BigDecimal temp;
        BigDecimal tempValorTotal = BigDecimal.ZERO;
        try {
            for (int cod : cods) {
                EnP1mFacturaVentaCab f = (new EnP1mFacturaVentaCabDao()).getById(cod);
                String myDateEmi = converter.format(f.getFacVenCabFecEmi());
                String myDateVen = converter.format(f.getFacVenCabFecVen());

                cliNom = f.getEnP1mCliente().getCliRazSoc();
                cliDir = f.getEnP1mCliente().getCliDir();
                fecEmi = myDateEmi;
                bol.writeBolSobCab(cliNom, cliDir, fecEmi);

                cliCod = f.getEnP1mCliente().getCliCod();
                conPag = f.getTaGzzMetodoPagoFactura().getMetPagDet();
                fecVen = myDateVen;

                try {
                    venRut = Integer.toString(f.getEnP1mCatalogoRuta().getCatRutCod());
                } catch (Exception e2) {
                    venRut = "-";
                }

                pdv = " ";

                try {
                    String tempDNI = (new EnP1mDocumentoClienteDao()).getById(new EnP1mDocumentoClienteId(cliCod, 1)).getDocCliNum();
                    if (!tempDNI.trim().equals("")) {
                        cliDni = "DNI: " + tempDNI;
                    } else {
                        cliDni = "";
                    }

                } catch (Exception ee) {
                    cliDni = "-";
                }
                obs = f.getFacVenCabObs() + cliDni;

                bol.writeBolCabecera(cliCod, conPag, fecVen, venRut, pdv, obs);

                proNum = 0;
                List<EnP1tFacturaVentaDet> detalles = (new EnP1mFacturaVentaCabDao()).getFacVenDets(cod);

                for (EnP1tFacturaVentaDet d : detalles) {
                    proNum++;
                    proCod = d.getEnP2mProducto().getId().getProCod();
                    proCan = BigDecimalUtil.get(d.getFacVenDetCan(), 0);
                    proUni = d.getEnP2mProducto().getTaGzzUnidadMed().getUniMedSim();
                    proDes = d.getEnP2mProducto().getProDet();
                    proValUni = BigDecimalUtil.get(d.getFacVenDetValUni(), 4);
                    proDes1 = Integer.toString(f.getFacVenPorDes());
                    temp = BigDecimalUtil.multiplicar(d.getFacVenDetCan(), d.getFacVenDetValUni(), empNumDec);
                    proValNet = BigDecimalUtil.get(temp, 4);
                    tempValorTotal = BigDecimalUtil.sumar(tempValorTotal, temp);
                    bol.writeBolDetalle(tipCod, proNum, proCod, proCan, proUni, proDes, proValUni, proDes1, df.format(proValNet));
                }
                bol.addLines(e.getEmpNumDetBolVen() - proNum);
                total = BigDecimalUtil.get(tempValorTotal, 4);
                bol.writeBolTotal(df.format(total));
                total = 0.0;
            }
            bol.close();
        } catch (IOException ex) {
        }
        String params[] = {file, bol.getName()};
        return params;
    }

    public String[] generateGuiaRemision(int[] cods) {

        String file = "Remision_" + sf.format(date.getTime()) + extension;
        ImpresoraMatricial rem = new ImpresoraMatricial(file, path, "remision");

        punPar = myEmpresa.getEmpDir();

        EnP1mCliente cliente;
        EnP2mTransportista transporista;
        EnP2mUnidadTransporte unidadTransporte;
        LogEntGuiaTransportistaCab guiaTra;

        try {
            for (int cod : cods) {
                guiaTra = guiTraCabDao.getById(cod);

                if (guiaTra == null) {
                    traNom = " ";
                    traLic = " ";
                    traPla = " ";
                    cliCod = " ";
                    cliNom = " ";
                    punLle = " ";
                    ven = " ";
                    fecVen = " ";
                } else {
                    transporista = guiaTra.getEnP2mTransportista();
                    traNom = transporista.getTraNom() + " "
                            + transporista.getTraApeMat() + " "
                            + transporista.getTraApePat();

                    try {
                        traLic = (new EnP2mDocumentoTransportistaDao()).getById(new EnP2mDocumentoTransportistaId(3, transporista.getTraCod())).getDocTraNum();
                    } catch (Exception e2) {
                        traLic = "-";
                    }

                    unidadTransporte = guiaTra.getEnP2mUnidadTransporte();
                    try {
                        traPla = unidadTransporte.getUniTraNumPla() + " / "
                                + unidadTransporte.getUniTraMar() + " / "
                                + unidadTransporte.getUniTraMod();
                    } catch (Exception e3) {
                        traPla = "-";
                    }

                    List<LogCtrGuiaTransportistaFacturas> guiaTransportistaFactura = new ArrayList<>(guiaTra.getLogCtrGuiaTransportistaFacturases());

                    if (guiaTransportistaFactura.size() > 1) {
                        cliCod = "-";
                        cliNom = "Varios";
                        cliRUC = "-\n";

                    } else if (guiaTransportistaFactura.size() == 1) {
                        cliente = guiaTransportistaFactura.get(0).getEnP1mFacturaVentaCab().getEnP1mCliente();
                        cliCod = cliente.getCliCod();

                        if (cliente.getTaGzzTipoCliente().getTipCliCod() == 1) {
                            cliNom = cliente.getCliNom() + " " + cliente.getCliApePat() + " " + cliente.getCliApePat();
                        } else if (cliente.getTaGzzTipoCliente().getTipCliCod() == 2) {
                            cliNom = cliente.getCliRazSoc();
                        }

                        try {
                            cliRUC = (new EnP1mDocumentoClienteDao()).getById(new EnP1mDocumentoClienteId(cliCod, 2)).getDocCliNum();
                            if (!cliRUC.trim().equals("")) {
                                cliRUC = "RUC: " + cliRUC + "\n";
                            }
                        } catch (Exception e4) {
                            cliRUC = "-\n";
                        }
                    }
                    punLle = guiaTra.getEnP1mCatalogoRuta().getCatRutDet();
                    ven = guiaTra.getEnP1mUsuario().getUsuCod();

                    String myDateEmi = converter.format(guiaTra.getGuiTraCabFecEmi());
                    String myDateVen = converter.format(guiaTra.getGuiTraCabFecVen());
                    fecVen = myDateVen;
                }

                zon = "";
                con = "Contado";
                oc = "";
                facNum = "";
                numInt = "";

                hora = DateUtil.getHoraActual();

                rem.writeGuiRemSobCab(cliNom, cliRUC, punPar, punLle, traNom, traLic, traPla);
                rem.writeGuiRemCabecera(fecVen, ven, zon, con, cliCod, oc, facNum, hora, numInt);

                List<LogCtrGuiaTransportistaDet> detalles = new ArrayList<>();
                detalles.addAll(guiaTra.getLogCtrGuiaTransportistaDets());

                proNum = 0;
                for (LogCtrGuiaTransportistaDet det : detalles) {
                    proNum++;
                    proCod = det.getEnP2mProducto().getId().getClaProCod();
                    proCan = BigDecimalUtil.get(det.getGuiTraDetNumItm());
                    proUni = det.getEnP2mProducto().getTaGzzUnidadMed().getUniMedSim();
                    proDes = det.getEnP2mProducto().getProDet();
                    proValUni = 0.0;
                    proDes1 = "";
                    proValNet = proCan * proValUni;
                    rem.writeGuiRemDetalle(tipCod, proNum, proCod, proCan, proUni, proDes, proDes1);
                }

                rem.addLines(myEmpresa.getEmpNumDetGuiRemTra() - proNum);

                motTra = guiaTra.getTaGzzMotivoTraslado().getMotTraCod().toString();
                rem.writeGuiRemMotTra(motTra);
            }
            rem.close();
        } catch (IOException e1) {
        }
        String params[] = {file, rem.getName()};
        return params;
    }

    //La impresora debe ser un recursos compartido
    public void sendToPrinter(File f, String printerName) {
        try {
            //String printerName;
            String ipAddress = "localhost";
            String file = f.getAbsolutePath();

            //PrintService printer = PrintServiceLookup.lookupDefaultPrintService();
            //printerName = printer.getName();
            //String printerDevice = "\\\\" + ipAddress + "\\" + printerName;
            String printCmd = "print /d:\\\\" + ipAddress + "\\" + printerName + " \"" + file + "\"";
            Process proceso = Runtime.getRuntime().exec(printCmd);
        } catch (IOException ex) {
            Logger.getLogger(Impresora.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
