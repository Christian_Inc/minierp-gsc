package org.epis.minierp.model;
// Generated 19/03/2017 04:36:54 PM by Hibernate Tools 4.3.1



/**
 * LogCtrGuiaTransportistaDetId generated by hbm2java
 */
public class LogCtrGuiaTransportistaDetId  implements java.io.Serializable {


     private int guiTraCabId;
     private int guiTraDetId;

    public LogCtrGuiaTransportistaDetId() {
    }

    public LogCtrGuiaTransportistaDetId(int guiTraCabId, int guiTraDetId) {
       this.guiTraCabId = guiTraCabId;
       this.guiTraDetId = guiTraDetId;
    }
   
    public int getGuiTraCabId() {
        return this.guiTraCabId;
    }
    
    public void setGuiTraCabId(int guiTraCabId) {
        this.guiTraCabId = guiTraCabId;
    }
    public int getGuiTraDetId() {
        return this.guiTraDetId;
    }
    
    public void setGuiTraDetId(int guiTraDetId) {
        this.guiTraDetId = guiTraDetId;
    }


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof LogCtrGuiaTransportistaDetId) ) return false;
		 LogCtrGuiaTransportistaDetId castOther = ( LogCtrGuiaTransportistaDetId ) other; 
         
		 return (this.getGuiTraCabId()==castOther.getGuiTraCabId())
 && (this.getGuiTraDetId()==castOther.getGuiTraDetId());
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + this.getGuiTraCabId();
         result = 37 * result + this.getGuiTraDetId();
         return result;
   }   


}


