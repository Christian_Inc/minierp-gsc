# README #

Instalacion:

Instalar Java jdk-8u131

Instalar mySql (workbeanch, wamp, xamp, heidi, etc)

        - Instalar la base de datos minierp1.14.sql
        - Instalar Carga Inicial.sql
        - Instalar Datos de prueba.sql
        - Crear Usuario id:admin pass:admin

Instalar apache-Tomcat versiones superiores a la 8.0

        - Copiar minierp.war en la carpeta webapps
        - puede cambiar el puerto si es necesario en conf/server.xml

Funcionalidad:

        - centrado a facturacion y boleteo
        - parte contable falta implementar
        - kardex falta corregir
        - 50% de las vistas falta completar paso al nuevo modelo visual
        - Sistema de impresión 80% terminado
        - La impresora debe ser un recursos compartido y debe estar conectada al servidor
